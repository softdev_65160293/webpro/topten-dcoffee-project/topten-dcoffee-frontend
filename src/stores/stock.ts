import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import type { Stock } from '@/types/Stock'
import type { ReceiptStockItem } from '@/types/ReceiptStockItem'
import type { ReceiptStock } from '@/types/ReceiptStock'
import { useAuthStore } from './auth'
import { useCheckStockStore } from './checkstock'
import type { CheckStock } from '@/types/CheckStock'
import { useLoadingStore } from './loading'
import stockService from '@/services/stock'
import receiptStockService from '@/services/receiptstock'
import Swal from 'sweetalert2'

export const useStockStore = defineStore('stock', () => {
  const loadingStore = useLoadingStore()
  const dialogS = ref(false)
  const formS = ref(false)
  const dialogE = ref(false)
  const dialogDelete = ref(false)
  const dialogCheckStock = ref(false)
  const dialogPrint = ref(false)
  const dialogImport = ref(false)
  const dialogShowReceipt = ref(false)
  const dialogImportPrint = ref(false)
  const authStore = useAuthStore()
  const textDate = ref()
  const dialogRe = ref(false)
  const checkStockStore = useCheckStockStore()
  const search = ref('')
  const dialogShowReceiptOne = ref(false)
  const dialogReport = ref(false)
const dialogReportCheckStock = ref(false)
  const stockItems = ref<Stock[]>([])
  const initSProduct: Stock = {
    name: '',
    minimum: 0,
    balance: 0,
    unit: '',
    price: 0,
    branchId: authStore.getCurrentUser()?.branch?.id!
  }

  const editedSProduct = ref<Stock>(JSON.parse(JSON.stringify(initSProduct)))
  const stockLows = ref<Stock[]>([])
  const stockChangeB = ref<Stock[]>([])
  const stockChangeC = ref<Stock[]>([])
  const editedLows = ref<Stock>(JSON.parse(JSON.stringify(initSProduct)))
  let editedIndex = -1
  const lastIndex = 15
  const unitchange = ref(0)
  const listCheck: number[] = Array.from({ length: stockItems.value.length }, () => 0)

  const receiptStockS = ref<ReceiptStock[]>([])
  // const receipt = ref<ReceiptStock>()
  const receiptStockItems = ref<ReceiptStockItem[]>([])
  const listReceipt: number[] = Array.from({ length: stockItems.value.length }, () => 0)
  const listBalance: number[] = Array.from({ length: stockItems.value.length }, () => 0)
  const listAmount: number[] = Array.from({ length: stockItems.value.length }, () => 0)

  let lastIdShow = receiptStockS.value.length + 1
  const date = ref()
  const formattedDate = ref<string>('')
  const receiptStock = ref<ReceiptStock>({
    idreceipt: '',
    nameplace: '',
    Date: '',
    total: 0,
    userId: authStore.getCurrentUser()?.id!,
    branchId: authStore.getCurrentUser()?.branch?.id!
  })
  clear()
  authStore.getCurrentUser()

  async function getStock(id: number) {
    try {
      loadingStore.doLoad()
      const res = await stockService.getStock(id)
      editedSProduct.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function getStocks() {
    const user = authStore.getCurrentUser()!
    const branchId = authStore.getCurrentUser()?.branch?.id!
    console.log('BRID' + branchId)
    console.log(user)
    try {
      loadingStore.doLoad()
      if (user.role == 'Staff' || user.role == 'Manager') {
        const res = await stockService.getStocksByBranch(branchId)
        stockItems.value = res.data
        loadingStore.finish()
      } else {
        const res = await stockService.getStocks()
        stockItems.value = res.data
        loadingStore.finish()
      }
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function addStock() {
    try {
      loadingStore.doLoad()
      const stock = editedSProduct.value
      if (!stock.id) {
        console.log('Post ' + JSON.stringify(stock))
        const res = await stockService.addStock(stock)
      } else {
        console.log('Patch ' + JSON.stringify(stock))
        console.log('id ' + stock.id)

        const res = await stockService.updateStock(stock)
      }
      await getStocks()
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function deleteStock(stock: Stock) {
    try {
      loadingStore.doLoad()
      stock = editedSProduct.value
      console.log(stock.id)
      const res = await stockService.delStock(stock)
      console.log(res)
      await getStocks()
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
    editedSProduct.value = initSProduct
    dialogDelete.value = !dialogDelete.value
    loadingStore.finish()
  }

  function editSProduct(s: Stock) {
    console.log('Open Edit dialog')
    editedIndex = stockItems.value.indexOf(s)
    editedSProduct.value = Object.assign({}, s)
    dialogE.value = true
    formS.value = true
  }
  function addreceiptStock() {
    console.log('Push ')
    // saveShow()
    setDate()
    receiptStock.value.id = lastIdShow++
    receiptStock.value.user = authStore.getCurrentUser()!
    receiptStock.value.Date = formattedDate.value
    textDate.value = receiptStock.value.Date.split(',')
    receiptStockS.value.unshift(receiptStock.value)
    console.log('ReceiptS' + receiptStockS.value)

    console.log('ReceiptSTOCK ' + receiptStock.value.receiptStockItems)
    console.log('User in re ' + receiptStock.value.user)
  }

  function showReceiptImport() {
    setDate()
    receiptStock.value.Date = formattedDate.value
    textDate.value = receiptStock.value.Date.split(',')
    dialogRe.value = !dialogRe.value
  }

  async function deleteSProduct(s: Stock) {
    editedSProduct.value = s
    console.log('data ' + JSON.stringify(editedSProduct.value))
    console.log('id ' + JSON.stringify(editedSProduct.value.id!))

    dialogDelete.value = true
  }
  function closeDelete() {
    console.log('Closing dialog')
    dialogDelete.value = false
    nextTick(() => {
      editedSProduct.value = Object.assign({}, initSProduct)
    })
  }
  function closeDialog() {
    console.log('Close')
    dialogE.value = !dialogE.value

    nextTick(() => {
      editedSProduct.value = Object.assign({}, initSProduct)
      editedIndex = -1
    })
  }
  function closeDialogT() {
    console.log('Close')
    editedSProduct.value = Object.assign({}, initSProduct)
    editedIndex = -1
    dialogE.value = !dialogE.value
  }

  function closeDialogAdd() {
    dialogS.value = false

    nextTick(() => {
      editedSProduct.value = Object.assign({}, initSProduct)
      editedIndex = -1
    })
  }
  function closeCheckDialog() {
    console.log('CloseCheck')
    dialogCheckStock.value = !dialogCheckStock.value
    nextTick(() => {
      editedLows.value = Object.assign({}, initSProduct)
      editedIndex = -1
    })
  }
  function deleteItemConfirm() {
    console.log('Delete item')
    // delete item from List

    closeDelete()
  }

  function save() {
    addStock()
    Swal.fire({
      icon: "success",
      title: "Your work has been saved",
      showConfirmButton: false,
      timer: 1500
    })
  }

  function openAddDialog() {
    dialogS.value = !dialogS.value
  }
  function minimumStock() {
    stockLows.value = []
    for (let index = 0; index < stockItems.value.length; index++) {
      if (stockItems.value[index].balance < stockItems.value[index].minimum) {
        stockLows.value.push(stockItems.value[index])
      }
    }
  }

  function setDate() {
    date.value = new Date()
    formattedDate.value = date.value.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false // 24 ชั่วโมง
    })
    textDate.value = ref(formattedDate.value.split(','))
  }

  function addBalance() {
    console.log('Add balance')
    for (let index = 0; index < listReceipt.length; index++) {
      const u = Number(listReceipt[index])
      stockItems.value[index].balance = u
    }
  }

  function editCheck(s: Stock) {
    editedIndex = stockItems.value.indexOf(s)
    editedSProduct.value = Object.assign({}, s)
  }
  function saveCheck() {
    console.log('Closed man')
    Object.assign(stockItems.value[editedIndex], editedSProduct.value)
    closeCheckDialog()
  }
  function logStupid() {
    console.log(listCheck)
  }
  function saveCheckStock() {}
  // function addStockinC() {
  //   for (let index = 0; index < checkStockStore.checkStocks.length; index++) {
  //     checkStockStore.checkStocks[index].idStock =
  //       checkStockStore.checkStocks[index].stock![index].id
  //     checkStockStore.checkStocks[index].minimum =
  //       checkStockStore.checkStocks[index].stock![index].minimum
  //     checkStockStore.checkStocks[index].name =
  //       checkStockStore.checkStocks[index].stock![index].name
  //     checkStockStore.checkStocks[index].price =
  //       checkStockStore.checkStocks[index].stock![index].price
  //     checkStockStore.checkStocks[index].unit =
  //       checkStockStore.checkStocks[index].stock![index].unit
  //   }
  //   console.log('Function addStock', checkStockStore.checkStocks)
  // }
  function BtoC(c: CheckStock) {
    for (let index = 0; index < listBalance.length; index++) {
      listBalance[index] = stockChangeB.value[index].balance
      c.stock![index].balance = Number(listBalance[index])
    }
    console.log('Add to c', c.stock!)
  }
  function newStocks() {
    const updatedStockItems = JSON.parse(JSON.stringify(stockItems.value))
    stockChangeC.value = []
    stockChangeC.value.push(updatedStockItems)
    console.log('ChangeC', stockChangeC)
  }

  function addB() {
    for (let index = 0; index < listBalance.length; index++) {
      const u = Number(listCheck[index])
      stockItems.value[index].balance = u
    }
  }

  function openPrint() {
    dialogCheckStock.value = !dialogCheckStock.value
    dialogPrint.value = !dialogPrint.value
  }
  function openCheckDialog() {
    newStocks()
    dialogCheckStock.value = !dialogCheckStock.value
  }
  function openImportDialog() {
    dialogImport.value = !dialogImport.value
    minimumStock()
  }
  const dialogImportM = ref(false)
  function openImportManage() {
    dialogImport.value = !dialogImport.value
    dialogImportM.value = !dialogImportM.value

    minimumStock()
  }

  //Import Stock-------------------------------------------------------
  function addStockItem(s: Stock) {
    console.log(s)
    const index = receiptStockItems.value.findIndex(
      (item) =>
        item.stock?.name === s.name &&
        item.stock?.minimum === s.minimum &&
        item.stock?.price === s.price &&
        item.stock?.unit === s.unit
    )
    if (index >= 0) {
      receiptStockItems.value[index].unitPerItem++
    } else {
      const newStockReceipt: ReceiptStockItem = {
        id: -1,
        name: s.name,
        minimum: s.minimum,
        balance: s.balance,
        unit: s.unit,
        price: s.price,
        totalprice: 0,
        unitPerItem: 1,
        stockId: s.id!,
        stock: s
      }
      receiptStockItems.value.push(newStockReceipt)
      calReceipt()
    }
  }
  function calReceipt() {
    let total = 0
    for (const item of receiptStockItems.value) {
      total = total + Number(item.totalprice)
    }
    receiptStock.value.total = total
  }
  async function getReceipt(id: number) {
    try {
      loadingStore.doLoad()
      const res = await receiptStockService.getReceiptStock(id)
      receiptStock.value = res.data
      loadingStore.finish()
      dialogShowReceiptOne.value = true
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function addReceiptStock() {
    console.log('addReceiptStock function')

    try {
      loadingStore.doLoad()

      // receiptStock.value.Date = formattedDate.value
      console.log('Receipt Date' + receiptStock.value.Date)
      console.log('TextDate' + textDate.value)
      console.log(receiptStockItems.value)

      receiptStock.value.userId = authStore.getCurrentUser()?.id!
      receiptStock.value.user = authStore.getCurrentUser()!
      console.log('UserId ' + receiptStock.value.userId)
      console.log('User' + receiptStock.value.user!)
      const receiptStock2 = receiptStock.value
      if (!receiptStock2.id) {
        console.log('Post ' + JSON.stringify(receiptStock))
        console.log('Date' + textDate.value[0])
        console.log('Total ' + receiptStock2.total)

        const res = await receiptStockService.addReceiptStock(
          receiptStock2,
          receiptStockItems.value,
          textDate.value
        )
      } else {
        console.log('Patch ' + JSON.stringify(receiptStock2))
        console.log('id ' + receiptStock2.id)

        const res = await receiptStockService.updateReceiptStock(receiptStock2)
      }
      await getReceipts()
      await getStocks()
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function getReceipts() {
    const user = authStore.getCurrentUser()!
    const branchId = authStore.getCurrentUser()?.branch?.id!
    console.log('BRID' + branchId)
    console.log(user)
    try {
      if (user.role == 'Staff' || user.role == 'Manager') {
        const res = await receiptStockService.getReceiptStocksByBranch(branchId)
        receiptStockS.value = res.data
        loadingStore.finish()
      } else {
        const res = await receiptStockService.getReceiptStocks()
        receiptStockS.value = res.data
        loadingStore.finish()
      }
    } catch (e) {
      loadingStore.finish()
    }
  }

  function enterPrice(s: ReceiptStockItem) {
    console.log('Enter', s.totalprice)
    const index = receiptStockItems.value.findIndex(
      (item) =>
        item.stock?.name === s.name &&
        item.stock?.minimum === s.minimum &&
        item.stock?.price === s.price &&
        item.stock?.unit === s.unit
    )
    receiptStockItems.value[index].totalprice = s.totalprice
    calReceipt()
  }

  function clearPrice(s: ReceiptStockItem) {
    // console.log('Enter',s.totalprice);
    const index = receiptStockItems.value.findIndex(
      (item) =>
        item.stock?.name === s.name &&
        item.stock?.minimum === s.minimum &&
        item.stock?.price === s.price &&
        item.stock?.unit === s.unit
    )
    receiptStockItems.value[index].totalprice = 0
  }

  function addBTest(s: ReceiptStockItem) {
    console.log('Enter Balance', s)
    const index = stockItems.value.findIndex((item) => item.id === s.stockId)
    receiptStock.value.receiptStockItems = receiptStockItems.value
    console.log('Index ' + index)
    stockItems.value[index].balance = s.unitPerItem
  }

  function clear() {
    receiptStockItems.value = []
    receiptStock.value = {
      idreceipt: '',
      nameplace: '',
      Date: '',
      total: 0,
      userId: authStore.getCurrentUser()?.id!,
      branchId: 0
    }
  }
  function closeImportM() {
    console.log('Close import M')
    dialogImportM.value = !dialogImportM.value
  }
  function clearListAmount() {
    listAmount.splice(0, listAmount.length)
  }
  return {
    stockItems,
    initSProduct,
    editedSProduct,
    dialogE,
    formS,
    dialogDelete,
    dialogS,
    dialogCheckStock,
    stockLows,
    unitchange,
    listCheck,
    dialogPrint,
    dialogImport,
    dialogImportM,
    receiptStockItems,
    receiptStock,
    receiptStockS,
    dialogShowReceipt,
    dialogImportPrint,
    textDate,
    dialogRe,
    listBalance,
    stockChangeB,
    stockChangeC,
    search,
    listAmount,
    dialogShowReceiptOne,
    dialogReport,
    dialogReportCheckStock,
    openImportManage,
    closeDelete,
    save,
    openAddDialog,
    deleteItemConfirm,
    deleteSProduct,
    editSProduct,
    closeDialogAdd,
    minimumStock,
    addBalance,
    saveCheck,
    editCheck,
    logStupid,
    saveCheckStock,
    openPrint,
    openImportDialog,
    openCheckDialog,
    addStockItem,
    enterPrice,
    clear,
    closeImportM,
    addBTest,
    addreceiptStock,
    closeDialog,
    closeDialogT,
    clearPrice,
    showReceiptImport,
    setDate,
    closeCheckDialog,
    BtoC,
    newStocks,
    getStock,
    getStocks,
    addStock,
    deleteStock,
    clearListAmount,
    addReceiptStock,
    getReceipt,
    getReceipts
  }
})
