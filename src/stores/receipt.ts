import { ref } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import receiptService from '@/services/receipt'
import userService from '@/services/user'
import { useAuthStore } from './auth'
import { useMembersStore } from './member'
import { usePromotionStore } from './promotion'
import { useLoadingStore } from './loading'
import auth from '@/services/auth'

export const useReceiptStore = defineStore('editedReceipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMembersStore()
  const loadingStore = useLoadingStore()
  const promotionStore = usePromotionStore()
  const dialog = ref(false)
  const receiptDialog = ref(false)
  const getBranchID = ref()
  const incomeYear = ref(0)
  const incomeMonth = ref(0)
  const incomeDay = ref(0)
  const activeComponent = ref(1)
  const date = ref()
  const formattedDate = ref<string>('')

  const textDate = ref()
  let unit = 0

  const initReceipt: Receipt = {
    date: '',
    total_before: 0,
    member_discount: 0,
    promotion_discount: 0,
    receiptItems: [],
    promotions: undefined,
    total: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: 'Cash',
    time: '',
    totalDiscount: 0,
    channel: 'Store',
    qty: 0,
    memberName: '',
  }
  const initReceiptItem: ReceiptItem = {
    name: '',
    price: 0,
    unit: 1,
    type: '-',
    gsize: 'S',
    sweet_level: '100%',
    productId: 0
  }
  const editedReceipt = ref<Receipt>(JSON.parse(JSON.stringify(initReceipt)))
  const editedReceiptItem = ref<ReceiptItem>(JSON.parse(JSON.stringify(initReceiptItem)))
  const receiptItems = ref<ReceiptItem[]>([])
  const receipts = ref<Receipt[]>([])
  function clearReceiptItem() {
    editedReceiptItem.value = JSON.parse(JSON.stringify(initReceiptItem))
  }

  async function getReceipts() {
    try {
      loadingStore.doLoad()
      authStore.getCurrentUser()
      const res = await receiptService.getReceipts()
      receipts.value = res.data
      for (const re of receipts.value) {
        re.user = (await userService.getUser(re.userId!)).data
      }
      console.log(receipts.value)

      loadingStore.finish()
    } catch (error) {
      console.log(error)
      loadingStore.finish()
    }
  }
  async function getReceipt(id: number) {
    loadingStore.doLoad()
    const res = await receiptService.getReceipt(id)
    receipts.value = res.data
    loadingStore.finish()
  }
  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }
  function calReceipt() {
    // promotionStore.disMax = 0
    // promotionStore.checkProductdis(receiptItems.value)
    // promotionStore.promotionUnit(receiptItems.value)
    // promotionStore.checkProductSetdis(receiptItems.value)
    let total_before = 0
    for (const item of receiptItems.value) {
      total_before += item.price * item.unit
    }
    editedReceipt.value.total_before = total_before
    editedReceipt.value.total = total_before
    promotionStore.setPromotion(receiptItems.value)
    editedReceipt.value.member_discount = parseInt(editedReceipt.value.member_discount.toString())
    editedReceipt.value.totalDiscount =
      editedReceipt.value.promotion_discount + editedReceipt.value.member_discount
    editedReceipt.value.total = total_before - editedReceipt.value.totalDiscount
  }
  function clearReceipt() {
    editedReceipt.value = JSON.parse(JSON.stringify(initReceipt))
    receiptItems.value = []
  }
  function clear() {
    memberStore.clear()
    changeComponent(1)
    receiptItems.value = []
    clearReceiptItem()
    promotionStore.promotionCurrent = []
    editedReceipt.value = JSON.parse(JSON.stringify(initReceipt))
    unit = 0
    memberStore.clearMember()
  }

  function showReceiptDialog() {
    setDate()
    editedReceipt.value.receiptItems = receiptItems.value
    editedReceipt.value.date = formattedDate.value
    textDate.value = editedReceipt.value.date.split(',')
    receiptDialog.value = true
  }
  function showReceiptDialog2(item: Receipt) {
    textDate.value = [item.date.substring(0, 10), item.time]
    receiptItems.value = item.receiptItems
    console.log(receiptItems.value)
    editedReceipt.value = item
    receiptDialog.value = true
  }

  function setDisMember(dis: number) {
    editedReceipt.value.member_discount = dis
    calReceipt()
  }
  function addPoint() {
    unit = 0
    for (const r of receiptItems.value) {
      unit += r.unit
    }
    memberStore.addPoint(unit)
  }
  function deletePoint() {
    memberStore.deletePoint(unit)
  }

  async function addReceipt() {
    try {
      loadingStore.doLoad()
      if (memberStore.currentMember != undefined) {
        editedReceipt.value.member = memberStore.currentMember
      }
      // if (promotionStore.currentPromotion != undefined) {
      //   editedReceipt.value.promotion = promotionStore.currentPromotion
      // }
      if (authStore.getCurrentUser() != null) {
        editedReceipt.value.user = authStore.getCurrentUser()!
      }
      // editedReceipt.value.id = lastidReceipt++
      editedReceipt.value.receiptItems = receiptItems.value
      editedReceipt.value.user = authStore.getCurrentUser()!
      if (isEmp()) {
        editedReceipt.value.branch = authStore.getCurrentUser()?.branch
      } else {
        console.log('Is Mem')
        console.log(editedReceipt.value)
        editedReceipt.value.user = undefined
        editedReceipt.value.member = authStore.getCurrentMember()!
      }

      console.log(editedReceipt.value)
      await receiptService.addReceipt(
        editedReceipt.value,
        receiptItems.value,
        textDate.value[0],
        textDate.value[1]
      )
      editedReceipt.value = JSON.parse(JSON.stringify(initReceipt))
      memberStore.currentMember = undefined
      promotionStore.promotionCurrent = []
      receiptItems.value = []
      clearReceiptItem()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
    }
  }

  function setDate() {
    date.value = new Date()
    formattedDate.value = date.value.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false // 24 ชั่วโมง
    })
    textDate.value = ref(formattedDate.value.split(','))
  }

  function closeDialog() {
    receiptDialog.value = false
    editedReceipt.value = JSON.parse(JSON.stringify(initReceipt))
  }

  function changeComponent(componentNumber: number) {
    activeComponent.value = componentNumber
  }
  function setIncome() {
    incomeYear.value = 0
    incomeMonth.value = 0
    incomeDay.value = 0
    for (const j of receipts.value) {
      const dateTime = j.date.split(',')
      const date = dateTime[0].split('/')
      if (date[2] === '2024') {
        incomeYear.value += j.total
      }
      if (date[0] === '1') {
        incomeMonth.value += j.total
      }
      if (date[1] === '15') {
        incomeDay.value += j.total
      }
    }
  }
  function closeDialogItem() {
    dialog.value = false
  }
  function selected() {
    const j = receiptItems.value.findIndex(
      (item) =>
        item.name === editedReceiptItem.value.name &&
        item.gsize === editedReceiptItem.value.gsize &&
        item.type === editedReceiptItem.value.type &&
        item.sweet_level === editedReceiptItem.value.sweet_level
    )
    console.log(j)
    if (j < 0) {
      receiptItems.value.push(editedReceiptItem.value)
      console.log(receiptItems.value)
    } else {
      receiptItems.value[j].unit++
    }
    dialog.value = false
    calReceipt()
    clearReceiptItem()
  }

  function pressProdcut(product: Product) {
    editedReceiptItem.value.product = product
    editedReceiptItem.value.productId = product.id!
    editedReceiptItem.value.name = product.name
    editedReceiptItem.value.price = product.price
    editedReceiptItem.value.type = product.type.charAt(0)
    console.log(editedReceiptItem.value)
    if (editedReceiptItem.value.product.category === 'Drink') {
      dialog.value = true
    } else {
      editedReceiptItem.value.gsize = '-'
      editedReceiptItem.value.type = '-'
      editedReceiptItem.value.sweet_level = '-'
      selected()
    }
  }
  async function getReceiptsByBranch(id: number) {
    try {
      loadingStore.doLoad()
      const res = await receiptService.getReceiptsByBranch(id)
      receipts.value = res.data
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
    }
  }

  async function getReceiptsByMember(memberId: number) {
    try {
      loadingStore.doLoad();
      const res = await receiptService.getReceiptsByMember(memberId);
      receipts.value = res.data;
      loadingStore.finish();
    } catch (error) {
      loadingStore.finish();
      // Handle error
    }
  }

  function isEmp() {
    if (
      authStore.getCurrentUser()?.role === 'Manager' ||
      authStore.getCurrentUser()?.role === 'Staff' ||
      authStore.getCurrentUser()?.role === 'Owner'
    ) {
      return true
    }
  }


  return {
    receiptItems,
    editedReceipt,
    receiptDialog,
    activeComponent,
    receipts,
    textDate,
    incomeYear,
    incomeMonth,
    incomeDay,
    removeReceiptItem,
    getReceiptsByMember,
    getReceiptsByBranch,
    inc,
    dec,
    calReceipt,
    clear,
    showReceiptDialog,
    setDisMember,
    addPoint,
    deletePoint,
    changeComponent,
    addReceipt,
    showReceiptDialog2,
    closeDialog,
    setIncome,
    editedReceiptItem,
    dialog,
    clearReceiptItem,
    closeDialogItem,
    selected,
    pressProdcut,
    getReceipts,
    clearReceipt,
    isEmp,
    getBranchID,
    getReceipt
  }
})
