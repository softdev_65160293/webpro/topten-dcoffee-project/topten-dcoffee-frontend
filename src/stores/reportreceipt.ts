import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import receiptService from '@/services/receipt'

export const usereprotReceiptStore = defineStore('reportreceipt', () => {
  const data = ref([])
  const labels = ref<any>([])
  const dataset = ref<any>([])
  const refreshkey = ref(0)
  const currentdate = ref('')

  async function reportYear(year: string) {
    const res = await receiptService.getDataYear(year)
    console.log(res.data)
    data.value = JSON.parse(JSON.stringify(res.data))
    console.log('Data Normal ' + data.value)
  }

  async function reportYearBranch(branchId: number) {
    const res = await receiptService.getDataYearBranch(branchId.toString())
    console.log(res.data)
    data.value = JSON.parse(JSON.stringify(res.data))
    console.log('Data Normal ' + data.value)
  }
  return { reportYear, reportYearBranch, labels, data, dataset, refreshkey, currentdate }
})
