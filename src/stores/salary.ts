import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import salaryService from '@/services/salary'
import type { Salary } from '@/types/Salary'
import { useCheckInStore } from './checkIn'
import { useUserStore } from './user'
import { type User } from '@/types/User'
import { useAuthStore } from './auth'

export const useSalaryStore = defineStore('salary', () => {
  const loadingStore = useLoadingStore()
  const checkInStore = useCheckInStore()
  const userStore = useUserStore()
  const authStore = useAuthStore()
  const salarys = ref<Salary[]>([])
  const salarysByUserId = ref<Salary[]>([])
  const salarysByBranchId = ref<Salary[]>([])
  const initialSalary: Salary = {
    payDate: new Date(),
    managerId: 0,
    paymentMethod: 'Credit Card',
    user: {
      id: 1,
      email: 'Dcoffee@gmail.com',
      username: 'admin',
      password: '1234',
      fullName: 'Admin Staff',
      gender: 'male',
      role: 'user',
      image: '',
      workRate: 0,
      workType: 'Part Time',
      paidedHour: 0,
      unpaidHour: 0
    },
    status: false
  }
  const editedSalary = ref<Salary>(JSON.parse(JSON.stringify(initialSalary)))

  async function getSalary(id: number) {
    loadingStore.doLoad()
    const res = await salaryService.getSalary(id)
    editedSalary.value = res.data
    loadingStore.finish()
  }

  async function getSalaryByUserId(id: number) {
    loadingStore.doLoad()
    const res = await salaryService.getSalaryByUserId(id)
    salarysByUserId.value = res.data
    loadingStore.finish()
  }

  async function getSalaryByBranchId(id: number) {
    loadingStore.doLoad()
    const res = await salaryService.getSalaryByBranchId(id)
    salarysByBranchId.value = res.data
    loadingStore.finish()
  }

  async function getSalarys() {
    try {
      loadingStore.doLoad()
      const res = await salaryService.getSalarys()
      salarys.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  const lastId = ref(0)

  async function CreateSalary(user: User) {
    loadingStore.doLoad()
    await checkInStore.getCheckIns()
    await getSalarys()
    await userStore.getUsers()
    const strUser = JSON.parse(localStorage.getItem('user') || '')
    const nowMaId = ref(0)
    const parsedUser = strUser
    nowMaId.value = parsedUser.id
    editedSalary.value.managerId = nowMaId.value
    editedSalary.value.user = user
    editedSalary.value.status = false
    const slaryE = editedSalary.value
    const res = await salaryService.addSalary(slaryE)
    getLastSalaryId()
    for (const check of await checkInStore.checkInList) {
      if (check.user.id === user.id && check.status == false) {
        check.salary === editedSalary.value
        check.salary?.id === editedSalary.value.id
        await checkInStore.updateCheck(check)
      }
    }

    await getSalarys()
    loadingStore.finish()
  }

  function getLastSalaryId() {
    getSalarys()
    if (salarys.value.length === 0) {
      return 0
    }
    const lastSalary = salarys.value[salarys.value.length - 1]
    console.log(salarys.value)

    lastId.value = lastSalary.id!

    console.log(editedSalary.value.id!)

    console.log('lastSalary' + lastSalary.id)
    editedSalary.value.id = Number(JSON.parse(JSON.stringify(lastSalary.id!)))
  }

  const salary = ref<Salary>()

  async function deleteSalary(user: User) {
    getSalarys()
    getLastSalaryId()
    loadingStore.doLoad()
    salary.value = editedSalary.value
    const res2 = await salaryService.delSalary(salary.value)

    await getSalarys()
    loadingStore.finish()
  }

  function clearForm() {
    editedSalary.value = JSON.parse(JSON.stringify(initialSalary))
  }

  const checkSalary = ref<Salary>()
  const booCheck = ref(false)

  async function findSalary(id: number) {
    const res = await salaryService.getSalaryByUserId(id)
    checkSalary.value = res.data
    if (checkSalary.value?.user?.id === id) {
      editedSalary.value = checkSalary.value
      booCheck.value = true
    } else if (checkSalary.value?.user?.id !== id) {
      booCheck.value = false
    }
  }

  async function updateStatus() {
    checkInStore.getCheckIns()
    for (const check of checkInStore.checkInList) {
      if (check.user.id === editedSalary.value.id && check.status == false) {
        check.status = true
        await checkInStore.updateCheck(check)
      }
    }
  }

  async function paidUpdate(item: User) {
    getLastSalaryId()
    checkInStore.getCheckIns()
    for (const check of checkInStore.checkInList) {
      if (check.user.id === item.id && check.status == false) {
        check.salary === editedSalary.value
        check.salary?.id === editedSalary.value.id
        check.status = true
        await checkInStore.updateCheck(check)
      }
    }

    const salary = editedSalary.value
    salary.id = editedSalary.value.id
    salary.status = true
    const res = await salaryService.updateSalary(salary)
  }

  return {
    salarys,
    getSalaryByUserId,
    getLastSalaryId,
    paidUpdate,
    booCheck,
    updateStatus,
    findSalary,
    getSalaryByBranchId,
    salarysByUserId,
    getSalarys,
    CreateSalary,
    deleteSalary,
    editedSalary,
    getSalary,
    clearForm
  }
})
