import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import reportreceiptstockService from '@/services/reportreceiptstock'
import type { AxiosResponse } from 'axios'
export const useReportStock = defineStore('reportstock', () => {
    const data = ref([])
    const labels =ref<any>([])
    const dataset =ref<any>([])
    const refreshkey = ref(0)
    const currentdate = ref('')
function updatedcurrentdate(){
  const today = new Date();
  const formattedDate = today.getFullYear() + ':' + (today.getMonth() + 1).toString().padStart(2, '0') + ':' + today.getDate().toString().padStart(2, '0');
  currentdate.value = formattedDate;
  console.log("Current Day of receiptStock "+currentdate.value);
  
}
async function reportDay(){
  
   const res = await reportreceiptstockService.reportDay(currentdate.value)
   console.log(res.data);
   data.value = JSON.parse(JSON.stringify(res.data))
   console.log("Data ReportDayCurrentDay Normal "+data.value);
   
   labels.value = res.data.map((item : any) =>{
    return item.item_name
   })
   console.log("labels "+labels.value);
  
   dataset.value = res.data.map((item : any) =>{
    return item.total
   })
  //  data.value = res.data

   console.log("Dataset "+dataset.value);
}

async function reportCurrent(){
  updatedcurrentdate()
  const res = await reportreceiptstockService.reportCurrent(currentdate.value)
  console.log(res.data);
  data.value = JSON.parse(JSON.stringify(res.data))
  console.log("Data ReportDayCurrentDay Normal "+data.value);
  
  labels.value = res.data.map((item : any) =>{
   return item.item_name
  })
  console.log("labels "+labels.value);
 
  dataset.value = res.data.map((item : any) =>{
   return item.total
  })
 //  data.value = res.data

  console.log("Dataset "+dataset.value);
}


async function reportDays(days:string){
  const res = await reportreceiptstockService.reportDay(days)
  console.log(res.data);
  data.value = JSON.parse(JSON.stringify(res.data))
  console.log("Data Normal "+data.value);
  
  labels.value = res.data.map((item : any) =>{
   return item.item_name
  })
  console.log("labels "+labels.value);
 
  dataset.value = res.data.map((item : any) =>{
   return item.total
  })
 //  data.value = res.data

  console.log("Dataset "+dataset.value);
  
}

async function reportMonth(month:string){
  const res = await reportreceiptstockService.reportMonth(month)
  console.log(res.data);
  data.value = JSON.parse(JSON.stringify(res.data))
  console.log("Data Normal "+data.value);
  
  labels.value = res.data.map((item : any) =>{
   return item.item_name
  })
  console.log("labels "+labels.value);
 
  dataset.value = res.data.map((item : any) =>{
   return item.total
  })
 //  data.value = res.data

  console.log("Dataset "+dataset.value);
  
}
async function reportYear(){
  const res = await reportreceiptstockService.reprotYear()
  console.log(res.data);
  data.value = JSON.parse(JSON.stringify(res.data))
  console.log("Data Normal "+data.value);
  
  // labels.value = res.data.map((item : any) =>{
  //  return item.item_name
  // })
  // console.log("labels "+labels.value);
 
  // dataset.value = res.data.map((item : any) =>{
  //  return item.total
  // })
 //  data.value = res.data

  // console.log("Dataset "+dataset.value);
  
}

  return { reportDay,reportMonth,reportDays,reportYear,data,dataset,labels,refreshkey,updatedcurrentdate,reportCurrent}
})
