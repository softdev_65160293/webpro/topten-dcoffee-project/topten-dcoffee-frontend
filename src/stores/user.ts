import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/user'
import type { User } from '@/types/User'
import { useBranchStore } from './branch'
import { useCheckInStore } from './checkIn'
import { useAuthStore } from './auth'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const branchStore = useBranchStore()
  const authService = useAuthStore()
  const checkInStore = useCheckInStore()
  type report = {
    id: number
    name: string
    email: string
    workType: string
    RowId: number
  }
  const reports = ref<report[]>([])
  const users = ref<User[]>([])
  const managers = ref<User[]>([])
  const userRoles = ref<User[]>([])
  const initialUser: User & { files: File[] } = {
    id: 0,
    email: '',
    username: '',
    password: '',
    fullName: '',
    gender: 'male',
    image: 'noimg.jpg',
    files: [],
    role: '',
    branch: undefined,
    workRate: 0,
    workType: '',
    paidedHour: 0,
    unpaidHour: 0,
    status: false
  }
  const editedUser = ref<User & { files: File[] }>(JSON.parse(JSON.stringify(initialUser)))
  const manager = ref<User & { files: File[] }>(JSON.parse(JSON.stringify(initialUser)))

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    editedUser.value = res.data
    checkInStore.getCheckIns()
    calWorkHour(1)
    loadingStore.finish()
  }

  async function getUserForSalary(id: number) {
    const res = await userService.getUser(id)
    manager.value = res.data
  }

  async function getUsers() {
    try {
      loadingStore.doLoad()
      const res = await userService.getUsers()
      users.value = res.data
      checkInStore.getCheckIns()
      calWorkHour(1)
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  function isManagerList(users: User) {
    return managers.value.some((item) => item.id === users.id)
  }

  function isUserList(users: User) {
    return userRoles.value.some((item) => item.id === users.id)
  }

  async function getManager() {
    try {
      const res = await userService.getUsers()
      users.value = res.data
      for (let i = 0; i < users.value.length; i++) {
        if (users.value[i].role === 'Manager' && !isManagerList(users.value[i])) {
          managers.value.push(users.value[i])
        }
      }
    } catch (e) {
      console.error(e)
    }
  }

  async function getRoleUser() {
    try {
      const res = await userService.getUsers()
      users.value = res.data
      for (let i = 0; i < users.value.length; i++) {
        if (users.value[i].role === 'Staff' && !isUserList(users.value[i])) {
          userRoles.value.push(users.value[i])
        }
      }
    } catch (e) {
      console.error(e)
    }
  }

  async function getUsersByBranch(branchId: number) {
    try {
      const res = await userService.getUsersByBranch(branchId)
      branchStore.userAtBranch = res.data
      checkInStore.getCheckIns()
      calWorkHour(0)
      // console.log(branchStore.userAtBranch)
    } catch (e) {
      console.error(e)
    }
  }

  async function saveUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    if (!user.id) {
      // Add new
      console.log('Post ' + JSON.stringify(user))
      const res = await userService.addUser(user)
      reports.value = res.data
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(user))
      const res = await userService.updateUser(user)
    }

    await getUsers()
    loadingStore.finish()
  }

  const etcUser = ref<User & { files: File[] }>()

  async function saveUserByNow(id: number) {
    loadingStore.doLoad()
    const user = editedUser.value
    if (!user.id) {
      // Add new
      console.log('Post ' + JSON.stringify(user))
      const res = await userService.addUser(user)
      reports.value = res.data
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(user))
      const res = await userService.updateUser(user)
      const res2 = await userService.getUser(id)
      etcUser.value = res2.data
      await localStorage.removeItem('user')
      await localStorage.setItem('user', JSON.stringify(etcUser.value))
    }

    await getUsers()
    loadingStore.finish()
  }

  async function deleteUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    const res = await userService.delUser(user)

    await getUsers()
    loadingStore.finish()
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialUser))
    getUsers()
  }

  async function calWorkHour(atStore: number) {
    await checkInStore.getCheckIns()
    const hourWork = ref(0)
    const hourPaid = ref(0)

    if (atStore == 1) {
      for (const check of checkInStore.checkInList) {
        for (const userC of users.value) {
          if (check.user.id === userC.id && check.status == false) {
            hourWork.value = hourWork.value + check.hour
            userC.unpaidHour = hourWork.value
          } else if (check.user.id === userC.id && check.status === true) {
            hourPaid.value = hourPaid.value + check.hour
            userC.paidedHour = hourPaid.value
          }
          if (userC.unpaidHour === 0 || userC.unpaidHour === undefined) {
            userC.status = true
          } else {
            userC.status = false
          }
        }
      }
    } else if (atStore === 0) {
      for (const check of checkInStore.checkInList) {
        for (const userC of branchStore.userAtBranch) {
          if (check.user.id === userC.id && check.status == false) {
            hourWork.value = hourWork.value + check.hour
            userC.unpaidHour = hourWork.value
          } else if (check.user.id === userC.id && check.status === true) {
            hourPaid.value = hourPaid.value + check.hour
            userC.paidedHour = hourPaid.value
          }
          if (userC.unpaidHour === 0 || userC.unpaidHour === undefined) {
            userC.status = true
          } else {
            userC.status = false
          }
        }
      }
    }

    hourWork.value = 0
    hourPaid.value = 0
  }

  return {
    etcUser,
    saveUserByNow,
    users,
    editedUser,
    managers,
    manager,
    getUserForSalary,
    userRoles,
    reports,
    getUsers,
    getRoleUser,
    saveUser,
    deleteUser,
    getUser,
    clearForm,
    getUsersByBranch,
    getManager
  }
})
