import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import memberService from '@/services/member'
import type { Member } from '@/types/Member'
import { useMessageStore } from './message'
import { useLoadingStore } from './loading'
import { useAuthStore } from './auth'
import { tr } from 'vuetify/locale'
import { useReceiptStore } from './receipt'
import Swal from 'sweetalert2'
import type { VForm } from 'vuetify/components'

export const useMembersStore = defineStore('member', () => {
  const messageStore = useMessageStore()
  const authStore = useAuthStore()
  const loadingStore = useLoadingStore()
  const receiptStore = useReceiptStore()

  const dis = ref()
  const initilMember: Member & { files: File[] } = {
    email: '',
    name: '',
    username: '',
    password: '',
    tel: '',
    point: 0,
    gender: '',
    image: 'noimage.jpg',
    files: []
  }

  const editedMember = ref<Member & { files: File[] }>(JSON.parse(JSON.stringify(initilMember)))
  const members = ref<Member[]>([])
  const currentMember = ref<(Member & { files: File[] }) | null>()

  const form = ref(false)
  const loading = ref(false)
  const notFoundAlert = ref(false)
  const dialogDelete = ref(false)
  const dialog = ref(false)

  const onSubmit = function () {}

  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedMember.value = Object.assign({}, initilMember)
    })
  }

  async function getMember(id: number) {
    loadingStore.doLoad()
    const res = await memberService.getMember(id)
    editedMember.value = res.data
    loadingStore.finish()
  }

  async function getMemberByTel(tel: string) {
    try {
      const res = await memberService.getMemberByTel(tel)
      if (res.data) {
        editedMember.value = res.data
      }
    } catch (error) {
      notFoundAlert.value = true
    }
    return editedMember.value
  }

  async function getMembers() {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMembers()
      members.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function saveMember() {
    try {
      loadingStore.doLoad()
      const member = editedMember.value
      if (!member.id) {
        console.log('Post ' + JSON.stringify(member))
        const res = await memberService.addMember(member)
        clearForm()
      } else {
        console.log('Patch ' + JSON.stringify(member))
        const res = await memberService.updateMember(member)
        clearForm()
      }
      await getMembers()
      dialog.value = false
      clearForm()
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function registerMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    if (!member.id) {
      // Add new
      const res = await memberService.addMember(member)
      authStore.login(member.username, member.password)
    } else {
      messageStore.errorDialog = true
    }

    await getMembers()
    loadingStore.finish()
  }

  function closeDelete() {
    dialogDelete.value = false
  }

  async function deleteMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    const res = await memberService.removeMember(member)

    await getMembers()
    loadingStore.finish()
    closeDelete()
  }

  async function deleteMemberConfirm(item: Member) {
    if (!item.id) return
    await getMember(item.id)
    dialogDelete.value = true
  }

  async function editMember(item: Member) {
    if (!item.id) return
    await getMember(item.id)
    dialog.value = true
  }

  function clearForm() {
    editedMember.value = JSON.parse(JSON.stringify(initilMember))
    getMembers()
  }

  function closeNotFound() {
    notFoundAlert.value = false
  }

  const searchMember = async (tel: string) => {
    try {
      loadingStore.doLoad()
      const res = await memberService.getMemberByTel(tel)
      currentMember.value = res.data
      receiptStore.editedReceipt.member = currentMember.value!
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: "Can't Find Member."
      })
    }
  }

  function clearMember() {
    currentMember.value = null
  }

  function clear() {
    editedMember.value = JSON.parse(JSON.stringify(initilMember))
    currentMember.value = null
  }

  function usePoint(point: number) {
    if (currentMember.value != null && currentMember.value != undefined) {
      dis.value = currentMember.value?.point
      currentMember.value.point = currentMember.value.point - point * 10
    }
  }

  function addPoint(point: number) {
    if (currentMember.value != null && currentMember.value != undefined)
      editedMember.value.point = 5 * point + currentMember.value.point
  }

  function deletePoint(point: number) {
    if (currentMember.value != null && currentMember.value != undefined)
      currentMember.value.point = currentMember.value.point - 5 * point
  }

  return {
    onSubmit,
    closeDialog,
    deleteMember,
    saveMember,
    getMember,
    closeDelete,
    registerMember,
    getMembers,
    clearForm,
    getMemberByTel,
    closeNotFound,
    deleteMemberConfirm,
    editMember,
    clearMember,
    clear,
    usePoint,
    addPoint,
    deletePoint,
    searchMember,
    currentMember,
    notFoundAlert,
    dialog,
    dialogDelete,
    form,
    loading,
    editedMember,
    members
  }
})
