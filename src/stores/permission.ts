import { defineStore } from 'pinia'
import router from '@/router'
import { useAuthStore } from './auth'

export const usePermissionStore = defineStore('permission', () => {
  const authStore = useAuthStore()

  function checkPermission() {
    if (isEmp()) {
      console.log('is emp')
    } else {
      console.log('no emp')
      router.push({ name: 'mainmenumember' })
    }
  }

  function pushEmp() {
    router.push({ name: 'mainmenu' })
  }

  function pushMember() {
    if (!isEmp()) {
      router.push({ name: 'mainmenumember' })
    }
  }

  function isEmp() {
    if (
      authStore.getCurrentUser()?.role === 'Owner' ||
      authStore.getCurrentUser()?.role === 'Manager' ||
      authStore.getCurrentUser()?.role === 'Staff'
    ) {
      return true
    }
  }
  return { checkPermission, isEmp, pushMember, pushEmp }
})
