import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import typebillcostService from '@/services/typebillcost'
import type { Typebillcost } from '@/types/Typebillcost'

export const useTypebillcostStore = defineStore('typebillcost', () => {
  const loadingStore = useLoadingStore()
  const typebillcosts = ref<Typebillcost[]>([])
  const initialTypebillcodt: Typebillcost = {
    name: ''
  }
  const editedType = ref<Typebillcost>(JSON.parse(JSON.stringify(initialTypebillcodt)))

  async function getTypebillcost(id: number) {
    loadingStore.doLoad()
    const res = await typebillcostService.getTypebillcost(id)
    editedType.value = res.data
    loadingStore.finish()
  }
  async function getTypebillcosts() {
    try {
      loadingStore.doLoad()
      const res = await typebillcostService.getTypebillcosts()
      typebillcosts.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveTypebillcost() {
    loadingStore.doLoad()
    const type = editedType.value
    if (!type.id) {
      // Add new
      console.log('Post ' + JSON.stringify(type))
      const res = await typebillcostService.addTypebillcost(type)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(type))
      const res = await typebillcostService.updateTypebillcost(type)
    }

    await getTypebillcosts()
    loadingStore.finish()
  }
  async function deleteType() {
    loadingStore.doLoad()
    const type = editedType.value
    const res = await typebillcostService.delTypebillcost(type)

    await getTypebillcosts()
    loadingStore.finish()
  }

  function clearForm() {
    editedType.value = JSON.parse(JSON.stringify(initialTypebillcodt))
  }
  return {
    typebillcosts,
    getTypebillcosts,
    saveTypebillcost,
    deleteType,
    editedType,
    getTypebillcost,
    clearForm
  }
})
