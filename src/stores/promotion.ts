import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { Promotion } from '@/types/Promotion'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useProductStore } from './product'
import { useReceiptStore } from './receipt'
import promotionService from '@/services/promotion'
import { useMessageStore } from './message'
import { useLoadingStore } from './loading'
import type { Product } from '@/types/Product'
export const usePromotionStore = defineStore('promotion', () => {
  const dialog = ref(false)
  const dialogType = ref(false)
  const dialogType2 = ref(false)
  const dialogType3 = ref(false)
  const dialogDelete = ref(false)
  const messageStroe = useMessageStore()
  const loadingStore = useLoadingStore()
  const productStore = useProductStore()
  const receiptStore = useReceiptStore()
  const product = ref()
  const search = ref('')
  const form = ref(false)
  const currentPromotion = ref<Promotion | null>()
  const discountPromotion = ref(0)
  const promotionCurrent = ref<Promotion[]>([])

  let editedIndex = -1

  const initPromotion: Promotion & { files: File[] } = {
    name: '',
    condition: '',
    start: '',
    end: '',
    discount: 0,
    products: [],
    image: 'noimage.jpg',
    files: []
  }

  const editedPromotion = ref<Promotion & { files: File[] }>(
    JSON.parse(JSON.stringify(initPromotion))
  )
  const promotions = ref<Promotion[]>([])
  async function getPromotions() {
    try {
      loadingStore.doLoad()
      const res = await promotionService.getPromotions()
      promotions.value = res.data
      loadingStore.finish()
    } catch (error) {
      console.log(error)
      loadingStore.finish()
    }
  }

  async function deletePromotion() {
    const rse = await promotionService.delPromotion(editedPromotion.value)
    getPromotions()
    clearForm()
    cleardialog()
  }

  function closeDialog() {
    dialogDelete.value = false
    nextTick(() => {
      editedPromotion.value = Object.assign({}, initPromotion)
      editedIndex = -1
    })
  }

  async function openDialogDelete(id: number) {
    const res = await promotionService.getPromotion(id)
    editedPromotion.value = res.data
    dialogDelete.value = true
  }

  async function editPromotion(id: number) {
    console.log(id)
    const res = await promotionService.getPromotion(id)
    clearForm()
    editedPromotion.value = res.data
    console.log(editedPromotion.value)
    console.log(editedPromotion.value.products.length)
    productStore.getProducts()
    if (editedPromotion.value.products.length > 1) {
      console.log(editedPromotion.value.products.map((product: Product) => product.name))
      product.value = editedPromotion.value.products.map((product: Product) => product.name)
      if (editedPromotion.value.discount > 1) {
        dialogType2.value = true
      } else {
        dialogType3.value = true
      }
    } else if (editedPromotion.value.discount < 1) {
      product.value = editedPromotion.value.products[0].name
      dialog.value = true
    }
  }

  function addPromotion() {
    dialog.value = true
  }

  async function save() {
    try {
      loadingStore.doLoad()
      const promotion = editedPromotion.value
      if (!promotion.id) {
        // Add new
        console.log('Post ' + JSON.stringify(promotion))
        const res = await promotionService.addPromotion(promotion)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(promotion))
        const res = await promotionService.updatePromotion(promotion)
      }
      await getPromotions()
      cleardialog()
      loadingStore.finish()
    } catch (error: any) {
      messageStroe.showMessage(error.message)
      loadingStore.finish()
    }
    closeDialog()
  }

  function cleardialog() {
    dialog.value = false
    dialogType.value = false
    dialogType2.value = false
    dialogType3.value = false
    dialogDelete.value = false
    clearForm()
    product.value = ''
  }
  function clearForm() {
    editedPromotion.value = JSON.parse(JSON.stringify(initPromotion))
  }
  function setPromotion(receiptItem: ReceiptItem[]) {
    discountPromotion.value = 0
    promotionCurrent.value = []
    const j = ref()
    promotions.value.sort((a, b) => b.discount - a.discount)
    for (const promo of promotions.value) {
      if (promo.status) {
        if (promo.products.length === 1) {
          j.value = receiptItem.some(
            (item: ReceiptItem) => item.product!.name === promo.products[0].name
          )
          if (j.value) {
            // const countDuplicates = receiptItem.filter(item => item.product!.name === promo.products[0].name).length;
            const indexes = receiptItem
              .map((item, index) => (item.product!.name === promo.products[0].name ? index : -1))
              .filter((index) => index !== -1)
            console.log('index found : ' + indexes)
            if (!isDuplicate(promotionCurrent.value!, promo)) {
              promotionCurrent.value.push(promo)
              for (const index of indexes) {
                discountPromotion.value +=
                  receiptItem[index].unit * (promo.products[0].price * promo.discount)
              }
              // discountPromotion.value += countDuplicates * (promo.products[0].price * promo.discount)
            }
          }
        } else {
          const ppName = promo.products.map((item) => item.name)
          const rpName: string[] = receiptItem.flatMap((re) => Array(re.unit).fill(re.name))
          console.log('ppName : ' + ppName)
          console.log('rpName : ' + rpName)
          const isEqual = ppName.every((value) => rpName.includes(value))
          if (isEqual) {
            const count = countMatches(ppName, rpName)
            console.log(count)

            promotionCurrent.value.push(promo)
            if (promo.discount > 1) {
              discountPromotion.value += count * promo.discount
            } else {
              const price = promo.products.reduce(
                (accumulator, currentValue) => accumulator + currentValue.price,
                0
              )
              discountPromotion.value += count * (price * promo.discount)
            }
          }
        }
      }
      receiptStore.editedReceipt.promotions = promotionCurrent.value
      receiptStore.editedReceipt.promotion_discount = discountPromotion.value
      console.log(receiptStore.editedReceipt)
    }
  }
  function countRounds(ppName: string[], rpName: string[]) {
    let count = 0
    for (const name of ppName) {
      if (rpName.includes(name)) {
        count++
      } else {
        count--
      }
    }

    return count
  }
  function countMatches(ppName: string[], rpName: string[]) {
    const repName = rpName
    const count = []

    for (const name of ppName) {
      let dd = 0
      for (let index = 0; index < repName.length; index++) {
        if (rpName[index] === name) {
          dd += 1
          repName.splice(index, 1)
          index--
        }
      }
      count.push(dd)
    }
    return Math.min(...count)
  }
  function isDuplicate(promotions: Promotion[], newPromotion: Promotion) {
    if (promotions !== undefined) {
      return promotions.some((promo) => promo.id === newPromotion.id)
    }
  }
  return {
    promotions,
    dialog,
    dialogDelete,
    form,
    editedPromotion,
    dialogType,
    dialogType2,
    dialogType3,
    product,
    editedIndex,
    search,
    deletePromotion,
    closeDialog,
    openDialogDelete,
    editPromotion,
    addPromotion,
    save,
    cleardialog,
    getPromotions,
    setPromotion,
    discountPromotion,
    promotionCurrent
  }
})
