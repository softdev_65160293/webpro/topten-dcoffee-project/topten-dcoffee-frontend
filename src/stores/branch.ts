import { useLoadingStore } from './loading'
import { ref } from 'vue'
import { defineStore } from 'pinia'
import branchService from '@/services/branch'
import type { Branch } from '@/types/Branch'
import type { User } from '@/types/User'

export const useBranchStore = defineStore('branch', () => {
  const loadingStore = useLoadingStore()
  const branchs = ref<Branch[]>([])
  const userAtBranch = ref<User[]>([])
  const initialBranch: Branch = {
    name: '',
    Address: '',
    tel: '',
    manager: {
      id: 0,
      username: '',
      email: '',
      image: 'noimage.jpg',
      password: '',
      fullName: '',
      gender: 'male',
      role: 'manager',
      branch: undefined,
      workRate: 0,
      workType: ''
    },
    billcosts: {
      id: undefined,
      typebillcost: { id: 1, name: 'Electricity bill' },
      user: undefined,
      date: '',
      time: '',
      billtotal: 0,
      branch: undefined,
      image: 'noimage.jpg'
    }
  }
  const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initialBranch)))

  const onSubmit = function () {}

  const form = ref(false)
  const dialogDelete = ref(false)
  const dialog = ref(false)

  async function getBranch(id: number) {
    const res = await branchService.getBranch(id)
    editedBranch.value = res.data
    loadingStore.finish()
  }

  async function getBranchs() {
    try {
      const res = await branchService.getBranchs()
      branchs.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function save() {
    loadingStore.doLoad()
    const branch = editedBranch.value
    if (!branch.id) {
      // Add new
      console.log('Post ' + JSON.stringify(branch))
      const res = await branchService.addBranch(branch)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(branch))
      const res = await branchService.updateBranch(branch)
    }
    closeDialog()
    await getBranchs()
    loadingStore.finish()
  }

  async function deleteBranch() {
    loadingStore.doLoad()
    const type = editedBranch.value
    const res = await branchService.delBranch(type)

    await getBranchs()
    loadingStore.finish()
  }

  async function deleteBranchConfirm(item: Branch) {
    if (!item.id) return
    await getBranch(item.id)
    dialogDelete.value = true
  }

  async function editBranch(item: Branch) {
    if (!item.id) return
    await getBranch(item.id)
    dialog.value = true
  }

  function closeDialog() {
    dialog.value = false
    clearForm()
  }

  function closeDelete() {
    dialogDelete.value = false
  }

  function clearForm() {
    editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
  }
  return {
    branchs,
    onSubmit,
    getBranchs,
    save,
    deleteBranch,
    userAtBranch,
    editedBranch,
    initialBranch,
    dialogDelete,
    dialog,
    form,
    deleteBranchConfirm,
    editBranch,
    getBranch,
    clearForm,
    closeDelete,
    closeDialog
  }
})
