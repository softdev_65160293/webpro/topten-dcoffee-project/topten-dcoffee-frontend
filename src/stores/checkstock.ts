import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

import { type CheckStock } from '@/types/CheckStock'
import { useAuthStore } from './auth'
import checkstockService from '@/services/checkstock'
import { useLoadingStore } from './loading'
import { useStockStore } from './stock'
import { type Stock } from '@/types/Stock'
import type { CheckStockItem } from '@/types/CheckStockItem'

export const useCheckStockStore = defineStore('checkstock', () => {

  const authStore = useAuthStore()
 const stockStore = useStockStore()
//  const stocks = stockStore.getStocks()
  const date = ref()
  const formattedDate = ref<string>('')
  const textDate = ref()
  const dialogHistory = ref(false)
  const dialogP = ref(false)
  const initcheckCheckStock: CheckStock= {
    userId:authStore.getCurrentUser()?.id!,
    date: '',
    checkStockItems:[],
    branchId:authStore.getCurrentUser()!.branch?.id!
  }
  const checkStockItems = ref<CheckStockItem[]>([])
  const newCheck = ref<CheckStock>()
  const loadingStore =useLoadingStore()
  const editedCheckstock = ref<CheckStock>(JSON.parse(JSON.stringify(initcheckCheckStock)))
  const checkStocks = ref<CheckStock[]>([])
  // const listAmount:number[] = Array.from({ length: stockStore.stockItems.length }, () => 0)

  // let lastIdcheck = checkCheckStocks.value.length + 1


  async function getCheckStock(id: number) {
    
    try {
      loadingStore.doLoad()
      const res = await checkstockService.getCheckStock(id)
      editedCheckstock.value = res.data
      dialogP.value =true
      loadingStore.finish()
    } catch (e) {
      loadingStore.fail()
    }
  }

  function addAmountToBalance() {
    console.log('addAmountToBalance function');
    
    for (let index = 0; index < stockStore.listAmount.length; index++) {
      const newcheckStockItem:CheckStockItem= {
        id: -1,
        name: stockStore.stockItems[index].name,
        minimum: stockStore.stockItems[index].minimum,
        balance: stockStore.stockItems[index].balance,
        unit: stockStore.stockItems[index].unit,
        price: stockStore.stockItems[index].price,
        stockId: stockStore.stockItems[index].id!,
        branchId:stockStore.stockItems[index].branch?.id!,
        branch:stockStore.stockItems[index].branch!,
        amount: 0
      } 
      const u = Number(stockStore.listAmount[index])
      newcheckStockItem.amount = u
      console.log(newcheckStockItem);
      
      checkStockItems.value.push(newcheckStockItem)
    }
    addCheckStock()
    stockStore.clearListAmount()
  }
  async function getCheckStocks() {
    const user = authStore.getCurrentUser()!
    const branchId = authStore.getCurrentUser()?.branch?.id!
    console.log('BRID' + branchId)
    console.log(user)
    try {
      if (user.role == 'Staff' || user.role == 'Manager') {
        const res = await checkstockService.getCheckStocksByBranch(branchId)
        checkStocks.value = res.data
        loadingStore.finish()
      } else {
        const res = await checkstockService.getCheckStocks()
        checkStocks.value = res.data
        loadingStore.finish()
      }
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function addCheckStock() {
    console.log('addCheckStock funtion');
    try {
      loadingStore.doLoad()
      const checkstock = editedCheckstock.value
      // for (let index = 0; index < stockStore.stockItems.length; index++) {
      //     checkstock.stock.push()
        
      // }
      // checkstock.stock =  await stockStore.getStocks()
      if (!checkstock.id) {
        console.log('Post ' + JSON.stringify(checkstock))
        const res = await checkstockService.addCheckStock(checkstock,checkStockItems.value)
      } else {
        console.log('Patch ' + JSON.stringify(checkstock))
        console.log("id " +checkstock.id);
        
        const res = await checkstockService.updateCheckStock(checkstock)
      }
      await getCheckStocks()
      await stockStore.getStocks()
      loadingStore.success()
    } catch (e) {
      loadingStore.fail()
    }
  }

  async function deleteCheckStock(checkstock:CheckStock) {
    try {
      loadingStore.doLoad()
      checkstock = editedCheckstock.value
      console.log(checkstock.id);
      const res = await checkstockService.delCheckStock(checkstock)
      console.log(res);
      await getCheckStocks()
      loadingStore.success()
    } catch (e) {
      loadingStore.fail()
    }
    editedCheckstock.value = initSProduct
    dialogDelete.value =!dialogDelete.value
    loadingStore.finish()
  }

  function logGG() {
    console.log('log ma doo', editedCheckstock.value.checkstock)
  }
  function setDate() {
    date.value = new Date()
    formattedDate.value = date.value.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false // 24 ชั่วโมง
    })
    textDate.value = ref(formattedDate.value.split(','))
  }
  function clear() {
    editedCheckstock.value =initcheckCheckStock
  }
  // function logIntplus(){
  //   for (let index = 0; index < checkstockStore.listCheck.length; index++) {
  //      const u =  Number(checkstockStore.listCheck[index])

  //      checkCheckStock.value[index].amount =u
  //   }

  // }
  function openDialogWithLoad(){
    dialogP.value = true
    loadingStore.doLoad()
    loadingStore.success()

  }
  return {
 
    initcheckCheckStock,
    checkStocks,
    dialogHistory,
    textDate,
    newCheck,
    editedCheckstock,
    dialogP,
    
    logGG,
    getCheckStock,
    getCheckStocks,
    addCheckStock,
    deleteCheckStock,
    checkStockItems,
    // listAmount,
    addAmountToBalance,
    openDialogWithLoad
  }
})
