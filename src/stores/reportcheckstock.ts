import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import reportreceiptstockService from '@/services/reportreceiptstock'
import reportcheckstockService from '@/services/reportcheckstock'
import type { AxiosResponse } from 'axios'

export const useReportCheckStock = defineStore('reportcheckstock', () => {
    const data = ref([])
    const labels =ref<any>([])
    const dataset =ref<any>([])
    const refreshkey = ref(0)
    const currentdate = ref('')
function updatedcurrentdate(){
  const today = new Date();
  const formattedDate = today.getFullYear() + ':' + (today.getMonth() + 1).toString().padStart(2, '0');
  currentdate.value = formattedDate;
  console.log(currentdate.value);
  
}

async function reportMonth(){
  updatedcurrentdate()
  const res = await reportcheckstockService.reportMonth(currentdate.value)
  console.log(res.data);
  data.value = JSON.parse(JSON.stringify(res.data))
  console.log("Data Normal Check Stock "+data.value);
  
  labels.value = res.data.map((item : any) =>{
   return item.item_name
  })
  console.log("labels "+labels.value);
 
  dataset.value = res.data.map((item : any) =>{
   return item.total
  })
 //  data.value = res.data

  console.log("Dataset "+dataset.value);
  
}

async function reportUsedMonth(month:string){
  const res = await reportcheckstockService.reportUsedMonth(month)
  console.log(res.data);
  data.value = JSON.parse(JSON.stringify(res.data))
  console.log("Data Normal "+data.value);
  
  labels.value = res.data.map((item : any) =>{
   return item.item_name
  })
  console.log("labels "+labels.value);
 
  dataset.value = res.data.map((item : any) =>{
   return item.total
  })
 //  data.value = res.data

  console.log("Dataset "+dataset.value);
  
}
async function reportUsedYear(year:string){
  const res = await reportcheckstockService.reprotUsedYear(year)
  console.log(res.data);
  data.value = JSON.parse(JSON.stringify(res.data))
  console.log("Data Normal "+data.value);
  
  // labels.value = res.data.map((item : any) =>{
  //  return item.item_name
  // })
  // console.log("labels "+labels.value);
 
  // dataset.value = res.data.map((item : any) =>{
  //  return item.total
  // })
 //  data.value = res.data

  // console.log("Dataset "+dataset.value);
  
}

  return {reportMonth,reportUsedMonth,reportUsedYear,data,dataset,labels,refreshkey}
})
