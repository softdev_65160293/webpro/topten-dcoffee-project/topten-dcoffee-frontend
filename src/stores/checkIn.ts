import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { CheckIn } from '@/types/CheckIn'
import { useAuthStore } from './auth'
import { useUserStore } from './user'
import checkInService from '../services/checkIn'
import userService from '../services/user'
import { useMessageStore } from './message'
import type { User } from '@/types/User'
import checkIn from '../services/checkIn'
import { useLoadingStore } from './loading'
import user from '@/services/user'
import type { Branch } from '@/types/Branch'

export const useCheckInStore = defineStore('checkIn', () => {
  const showCheckInButton = ref(true)
  const dropCheckInButton = ref(false)
  const dialogVisible = ref(false)
  const checkoneday = ref(false)
  const authStore = useAuthStore()
  const canClick = ref(true)
  const messageStore = useMessageStore()
  const checkOut = ref(false)
  const branch = ref<string>()
  const time = ref<string>('')
  const userCheckIn = ref<User>({
    email: '',
    username: 'admin',
    password: '1234',
    fullName: '',
    gender: '',
    role: '',
    image: '',
    workRate: 0,
    workType: '',
    paidedHour: 0,
    unpaidHour: 0
  })
  const date = ref()
  const formattedDate = ref<string>('')
  const textDate = ref()
  const initCheckIn: CheckIn = {
    user: authStore.getCurrentUser()!,
    checkIn: '',
    checkOut: '',
    date: '',
    hour: 0,
    status: false
  }
  const checkInOuts = ref<CheckIn[]>([])
  const editedCheckIn = ref<CheckIn>(JSON.parse(JSON.stringify(initCheckIn)))
  async function checkInTime() {
    setDateAndTime()
    console.log(formattedDate.value)
    console.log(time.value)
    editedCheckIn.value.date = formattedDate.value
    editedCheckIn.value.user = authStore.getCurrentUser()!
    if (checkOut.value) {
      editedCheckIn.value.checkOut = '17:00:00'
      try {
        const res = await checkInService.checkOut(editedCheckIn.value)
      } catch (error) {
        console.log('')
      }
    } else {
      editedCheckIn.value.checkIn = time.value
      try {
        const res = await checkInService.checkIn(editedCheckIn.value)
      } catch (error) {
        console.log('')
      }
    }
    editedCheckIn.value = JSON.parse(JSON.stringify(initCheckIn))

    // editedCheckIn.value.id = lastId++
    // editedCheckIn.value.checkIn = tt.value[1]
    // editedCheckIn.value.date = tt.value[0]
    // checkInOuts.value.unshift(editedCheckIn.value)
    // console.log(editedCheckIn.value)
  }
  function setDateAndTime() {
    date.value = new Date()
    formattedDate.value = date.value.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false // 24 ชั่วโมง
    })
    const tt = ref(formattedDate.value.split(','))
    const [month, day, year] = tt.value[0].split('/')
    formattedDate.value = `${year}-${month.padStart(2, '0')}-${day.padStart(2, '0')}`
    time.value = tt.value[1].split('/')[0].trim()
  }
  function checkOutTime() {
    date.value = new Date()
    formattedDate.value = date.value.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false // 24 ชั่วโมง
    })

    const tt = ref(formattedDate.value.split(','))
    const outTime = new Date(date.value)
    outTime.setHours(outTime.getHours() + 8) // บวก ชั่วโมง 8 ชั่วโมง

    const outTimeString = outTime.toLocaleTimeString('en-US', {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      hour12: false
    })

    checkInOuts.value[0].checkOut = outTimeString
    const inputParts = checkInOuts.value[0].checkIn.split(':')
    const inputHours = parseInt(inputParts[0], 10)
    const inputMins = parseInt(inputParts[1], 10)
    const outputParts = checkInOuts.value[0].checkOut.split(':')
    const outputHours = parseInt(outputParts[0], 10)
    const outputMins = parseInt(outputParts[1], 10)
    // ตรวจสอบว่า TimeOut มีค่าน้อยกว่า TimeIn หรือไม่
    let adjustedOutputHours = outputHours
    if (outputHours < inputHours || (outputHours === inputHours && outputMins < inputMins)) {
      adjustedOutputHours += 24 // เพิ่ม 24 ชั่วโมง
    }
    // คำนวณชั่วโมง
    const totalHours = adjustedOutputHours - inputHours
    checkInOuts.value[0].hour = totalHours
    // คำนวณค่าแรง
    editedCheckIn.value = initCheckIn
  }

  //     const tt = ref(formattedDate.value.split(','))

  //     const outTime = new Date(date.value);
  //     outTime.setHours(outTime.getHours() + 8); // บวก ชั่วโมง 8 ชั่วโมง

  //    const outTimeString = outTime.toLocaleTimeString('en-US', {
  //     hour: '2-digit',
  //     minute: '2-digit',
  //     second: '2-digit',
  //     hour12: false,
  //   });
  //     checkInOuts.value[0].checkOut = outTimeString
  //     // checkInOuts.value[0].checkOut = '15:00:00'
  //     const inputParts = checkInOuts.value[0].checkIn.split(':');
  // const inputHours = parseInt(inputParts[0], 10);
  // const inputmins = parseInt(inputParts[1], 10);
  // const outputParts = checkInOuts.value[0].checkOut.split(':');
  // const outputmins = parseInt(outputParts[1], 10);
  // const outputHours = parseInt(outputParts[0], 10);

  //     // แปลงระหว่างวินาทีเป็นชั่วโมง, นาที, และวินาที
  //     const mins = ref(0)
  //     if (inputmins > outputmins) {
  //       mins.value = 1
  //     }
  //     checkInOuts.value[0].hour = outputHours - inputHours - mins.value
  //     checkInOuts.value[0].total = checkInOuts.value[0].hour * 50
  //     editedCheckIn.value = initCheckIn
  //   }

  async function chkUser() {
    const ediedUser = (await userService.getUser(authStore.getCurrentUser()?.id!)).data
    console.log(ediedUser)
    console.log(userCheckIn.value)
    if (
      ediedUser?.username === userCheckIn.value?.username &&
      ediedUser?.password === userCheckIn.value?.password
    ) {
      console.log('chkUser')
      await checkInTime()
      showCheckInButton.value = false
      checkOut.value = false
      dialogVisible.value = false
      setTimeout(() => {
        canClick.value = true
      }, 10000) // 60000 milliseconds = 1 นาที
    } else {
      messageStore.text = 'Please enter correct information.'
      messageStore.snackbar = true
    }
    userCheckIn.value.username == ''
    userCheckIn.value.password == ''
  }

  const loadingStore = useLoadingStore()

  const checkInList = ref<CheckIn[]>([])
  const checkInByID = ref<CheckIn>()
  const checkinssById = ref<CheckIn[]>([])

  async function getCheckInById(id: number) {
    loadingStore.doLoad()
    const res = await checkInService.getCheckIn(id)
    checkInByID.value = res.data
    loadingStore.finish()
  }

  async function getCheckInsyByUserId(id: number) {
    const res = await checkInService.getCheckInByUserId(id)
    checkinssById.value = res.data
  }

  async function getCheckIns() {
    try {
      loadingStore.doLoad()
      const res = await checkInService.getCheckIns()
      checkInList.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function updateCheck(check: CheckIn) {
    try {
      console.log(check)
      const res = await checkInService.updateCheck(check)
    } catch (e) {
      console.log(e)
    }
  }

  return {
    checkInList,
    checkinssById,
    getCheckInsyByUserId,
    checkInByID,
    updateCheck,
    getCheckInById,
    getCheckIns,
    editedCheckIn,
    checkInTime,
    checkInOuts,
    checkOutTime,
    chkUser,
    showCheckInButton,
    dialogVisible,
    checkoneday,
    userCheckIn,
    checkOut,
    dropCheckInButton,
    branch,
    canClick,
    formattedDate,
    setDateAndTime
  }
})
