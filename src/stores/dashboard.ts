import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import { useBillCostStore } from './billcost'
import { useStockStore } from './stock'
import { useSalaryStore } from './salary'
import { useReceiptStore } from './receipt'
import { useAuthStore } from './auth'
import salaryService from '@/services/salary'
import stockService from '@/services/stock'
import type { ReceiptStock } from '@/types/ReceiptStock'
import type { Salary } from '@/types/Salary'

export const useDashBoradStore = defineStore('dashboard', () => {
  const billCostStore = useBillCostStore()
  const stockStore = useStockStore()
  const salaryStore = useSalaryStore()
  const receiptStore = useReceiptStore()
  const authStore = useAuthStore()
  const stocks = ref<ReceiptStock[]>([])
  const salarys = ref<Salary[]>([])

  const dialogLowStock = ref(false)
  //cost
  const totalCost = ref<number>(0)
  const billtotal = ref<number>(0)
  const orderStock = ref<number>(0)
  const salaryValue = ref<number>(0)

  //income
  const incomeValue = ref<number>(0)
  const profit = ref<number>(0)

  function refresh() {
    profit.value = 0
    incomeValue.value = 0
    totalCost.value = 0
  }

  async function calBill() {
    await billCostStore.getBillCosts()
    if (billCostStore.billCosts && billCostStore.billCosts.length > 0) {
      billtotal.value = billCostStore.billCosts
        .map((billCost: { billtotal: any }) => billCost.billtotal)
        .reduce((total: any, current: any) => total + current, 0)
    }
    totalCost.value += billtotal.value
  }

  async function calOrderStock() {
    await stockStore.getReceipts()
    if (stockStore.receiptStockS && stockStore.receiptStockS.length > 0) {
      orderStock.value = stockStore.receiptStockS
        .map((receiptStock) => receiptStock.total)
        .reduce((total: number, current: number) => total + current, 0)
    }
    totalCost.value += orderStock.value
  }

  async function calSalary() {
    await salaryStore.getSalarys()
    if (salaryStore.salarys && salaryStore.salarys.length > 0) {
      salaryValue.value = salaryStore.salarys
        .map((salary) => salary.totalPrice)
        .reduce((total: any, current: any) => total + current, 0)
    }
    totalCost.value += salaryValue.value
  }

  async function calReceipt() {
    await receiptStore.getReceipts()
    if (receiptStore.receipts && receiptStore.receipts.length > 0) {
      incomeValue.value = receiptStore.receipts
        .map((receipt) => receipt.total)
        .reduce((total: any, current: any) => total + current, 0)
    }
  }

  // Branch for manager
  async function calBillBranch() {
    await billCostStore.getBillCostsByBranch(authStore.getCurrentUser()?.branch?.id!)
    if (billCostStore.billCosts && billCostStore.billCosts.length > 0) {
      billtotal.value = billCostStore.billCosts
        .map((billCost: { billtotal: any }) => billCost.billtotal)
        .reduce((total: any, current: any) => total + current, 0)
    }
    totalCost.value += billtotal.value
  }

  async function calOrderStockBranch() {
    const stock = await stockService.getStocksByBranch(authStore.getCurrentUser()?.branch?.id!)
    stocks.value = stock.data
    if (stocks.value && stocks.value.length > 0) {
      orderStock.value = stocks.value
        .map((stock) => stock.total)
        .reduce((total: number, current: number) => total + current, 0)
    }
    totalCost.value += orderStock.value
  }

  async function calSalaryBranch() {
    const salary = await salaryService.getSalaryByBranchId(authStore.getCurrentUser()?.branch?.id!) 
    salarys.value = salary.data
    if (salarys.value && salarys.value.length > 0) {
        salaryValue.value = salarys.value
            .map((salary) => salary.totalPrice)
            .reduce((total: any, current: any) => total + current, 0)
    }
    totalCost.value += salaryValue.value
    console.log('salary' + salaryValue.value)
  }

  async function calReceiptBranch() {
    await receiptStore.getReceiptsByBranch(authStore.getCurrentUser()?.branch?.id!)
    if (receiptStore.receipts && receiptStore.receipts.length > 0) {
      incomeValue.value = receiptStore.receipts
        .map((receipt) => receipt.total)
        .reduce((total: any, current: any) => total + current, 0)
    }
    console.log('income' + incomeValue.value)
  }

  watch([incomeValue, totalCost], ([newIncomeValue, newTotalCost]) => {
    profit.value = newIncomeValue - newTotalCost
  })

  async function calTotal() {
    await calReceipt()
    await calBill()
    await calOrderStock()
    await calSalary()
  }

  async function calTotalBranch() {
    await calReceiptBranch()
    await calBillBranch()
    await calOrderStockBranch()
    await calSalaryBranch()
  }

  function closeLowStock() {
    dialogLowStock.value = false
  }

  return {
    calBill,
    calOrderStock,
    calSalary,
    calReceipt,
    refresh,
    closeLowStock,
    calTotal,
    calTotalBranch,
    dialogLowStock,
    billtotal,
    profit,
    orderStock,
    salaryValue,
    incomeValue,
    totalCost
  }
})
