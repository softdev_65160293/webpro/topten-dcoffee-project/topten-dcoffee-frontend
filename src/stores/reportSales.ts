import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import receiptService from '@/services/receipt'

export const usereportSalesStore = defineStore('reportSales', () => {
  const data = ref([])
  const labels = ref<any>([])
  const dataset = ref<any>([])
  const refreshkey = ref(0)
  const currentdate = ref('')

  async function dailySales(id: number, month: string) {
    const res = await receiptService.getSalesDaily(id, month)
    console.log(res.data)
    data.value = JSON.parse(JSON.stringify(res.data))
    console.log('Data Normal ' + data.value)
  }

  async function dailyMonth(id: number) {
    const res = await receiptService.getSalesMonth(id)
    console.log(res.data)
    data.value = JSON.parse(JSON.stringify(res.data))
    console.log('Data Normal ' + data.value)
  }

  async function dailyYear(id: number) {
    const res = await receiptService.getSalesYear(id)
    console.log(res.data)
    data.value = JSON.parse(JSON.stringify(res.data))
    console.log('Data Normal ' + data.value)
  }
  return { dailySales, dailyMonth, dailyYear, labels, data, dataset, refreshkey, currentdate }
})
