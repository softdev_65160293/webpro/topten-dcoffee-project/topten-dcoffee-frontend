import { createVuetify } from 'vuetify'

import colors from 'vuetify/util/colors'

export default createVuetify({
  theme: {
    themes: {
      light: {
        dark: false,
        colors: {
          primary: colors.orange.darken4,
          secondary: colors.red.lighten4
        }
      }
    }
  }
})
