
import type { CheckStock } from '@/types/CheckStock'
import http from './http'
import type { CheckStockItem } from '@/types/CheckStockItem';
import branch from './branch';
type CheckStockDTO={
    checkStockItems:{
        stockId:number;
        balance: number;
        amount: number;
    }[];
    userId: number;
    date: Date;
    branchId:number
}

function addCheckStock(checkstock: CheckStock,checkStockItems:CheckStockItem[]) {
   const checkStockDTO:CheckStockDTO = {
     checkStockItems: [],
     userId: 0,
     date: new Date,
     branchId:0
   }
   checkStockDTO.userId = checkstock.userId
   checkStockDTO.branchId = checkstock.branchId
   console.log("CHECK STOCK B"+checkStockDTO.branchId);
   
   checkStockDTO.checkStockItems = checkStockItems.map((item)=>{
    return {
      stockId:item.stockId,
      balance:item.balance,
      amount:item.amount
    }
   })
  return http.post('/checkstocks', checkStockDTO)
}

function updateCheckStock(checkstock: CheckStock) {
  return http.patch(`/checkstocks/${checkstock.id}`, checkstock)
}

function delCheckStock(checkstock: CheckStock) {
  return http.delete("/checkstocks/"+checkstock.id)
}

function getCheckStock(id: number) {
  return http.get(`/checkstocks/${id}`)
}

function getCheckStocks() {
  return http.get('/checkstocks')
}
function getCheckStocksByBranch(branchId:number) {
  return http.get('/checkstocks/getbranch/branch/'+branchId)
}

export default {
  addCheckStock,
  updateCheckStock,
  delCheckStock,
  getCheckStock,
  getCheckStocks,
  getCheckStocksByBranch
}
