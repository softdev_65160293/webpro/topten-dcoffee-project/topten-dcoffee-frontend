import type { Product } from '@/types/Product'
import http from './http'

function addProduct(product: Product & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', product.name)
  formData.append('price', product.price.toString())
  formData.append('type', product.type)
  formData.append('category', product.category)
  formData.append('gsize', product.gsize)
  formData.append('sweet_level', product.sweet_level)
  if (product.files && product.files.length > 0) {
    formData.append('file', product.files[0])
    console.log(product.files[0])
  }

  return http.post('/products', formData, {
    headers: {
      'content-Type': 'multipart/form-data'
    }
  })
}

function updateProduct(product: Product & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', product.name)
  formData.append('price', product.price.toString())
  formData.append('type', product.type)
  formData.append('category', product.category)
  formData.append('gsize', product.gsize)
  formData.append('sweet_level', product.sweet_level)
  if (product.files && product.files.length > 0) {
    formData.append('file', product.files[0])
    console.log(product.files[0])
  }
  return http.post(`/products/${product.id}`, formData, {
    headers: {
      'content-Type': 'multipart/form-data'
    }
  })
}

function delProduct(product: Product) {
  return http.delete(`/products/${product.id}`)
}

function getProduct(id: number) {
  return http.get(`/products/${id}`)
}
function getProductsByType(typeId: string) {
  return http.get('/products/category/' + typeId)
}
function getProducts() {
  return http.get('/products')
}

export default { addProduct, updateProduct, delProduct, getProduct, getProducts, getProductsByType }
