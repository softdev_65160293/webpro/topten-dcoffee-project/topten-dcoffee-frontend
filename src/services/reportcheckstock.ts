import http from './http'

function reportMonth(month:string) {
    return http.get('/checkstocks/reportMonth/Month/'+month)
  }
function reportUsedMonth(month:string) {
  return http.get('/checkstocks/reportUsedMonth/Month/'+month)
}
function reprotUsedYear(year:string) {
  return http.get('/checkstocks/reportUsedYear/Year/'+year)
}
export default { reportMonth, reportUsedMonth,reprotUsedYear}
