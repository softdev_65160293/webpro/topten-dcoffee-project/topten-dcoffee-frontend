import type { Stock } from '@/types/Stock'
import http from './http'
type StockDTO={
 name:string
 minimum:number
 balance:number
 price:number
 unit:string
 branchId:number
}
function addStock(stock: Stock) {
  const stocktockDTO:StockDTO = {
    name:'',
    minimum:0,
    balance:0,
    price:0,
    unit:'',
    branchId:0,
  }
  stocktockDTO.name = stock.name
  stocktockDTO.minimum = stock.minimum
  stocktockDTO.balance = stock.balance
  stocktockDTO.price = stock.price
  stocktockDTO.unit = stock.unit
  stocktockDTO.branchId = stock.branchId
  return http.post('/stocks', stocktockDTO)
}

function updateStock(stock: Stock) {
  return http.patch(`/stocks/${stock.id}`, stock)
}

function delStock(stock: Stock) {
  return http.delete("/stocks/"+stock.id)
}

function getStock(id: number) {
  return http.get(`/stocks/${id}`)
}

function getStocks() {
  return http.get('/stocks')
}

function getStocksByBranch(branchId:number) {
  return http.get('/stocks/branchId/'+branchId)
}
export default {
  addStock,
  updateStock,
  delStock,
  getStock,
  getStocks,
  getStocksByBranch
}
