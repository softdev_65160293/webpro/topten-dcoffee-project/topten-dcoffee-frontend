import type { User } from '@/types/User'
import http from './http'
import type { Branch } from '@/types/Branch'
import branch from './branch'

function addUser(user: User & { files: File[] }) {
  const formData = new FormData()
  formData.append('email', user.email)
  formData.append('username', user.username)
  formData.append('fullName', user.fullName)
  formData.append('gender', user.gender)
  formData.append('password', user.password)
  formData.append('role', user.role)
  formData.append('workRate', user.workRate.toString())
  formData.append('workType', user.workType)
  const branch: Branch | undefined = user.branch
  const id = branch?.id?.toString()
  if (branch) {
    formData.append('branchId', id || '')
    formData.append('branchName', branch.name)
    formData.append('branchAddress', branch.Address)
  }
  if (user.files && user.files.length > 0) {
    formData.append('file', user.files[0])
  }
  return http.post('/users', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateUser(user: User & { files: File[] }) {
  const formData = new FormData()
  formData.append('email', user.email)
  formData.append('username', user.username)
  formData.append('fullName', user.fullName)
  formData.append('gender', user.gender)
  formData.append('password', user.password)
  formData.append('role', user.role)
  formData.append('workRate', user.workRate.toString())
  formData.append('workType', user.workType)
  const branch: Branch | undefined = user.branch
  const id = branch?.id?.toString()
  if (branch) {
    formData.append('branchId', id || '')
    formData.append('branchName', branch.name)
    formData.append('branchAddress', branch.Address)
    formData.append('branchTel', branch.tel)
  }
  if (user.files && user.files.length > 0) {
    formData.append('file', user.files[0])
  }
  return http.post(`/users/${user.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delUser(user: User) {
  return http.delete(`/users/${user.id}`)
}

function getUsersByBranch(branchId: number) {
  return http.get(`/users/branch/${branchId}`)
}

function getUser(id: number) {
  return http.get(`/users/${id}`)
}

function getUsers() {
  return http.get('/users')
}
function getView() {
  return http.get('/users/view')
}
function whileUser(user: User) {
  type user2 = {
    id?: number
    email: string
    username: string
    password: string
    fullName: string
    branch?: Branch
    gender: string
    role: string
    image: string
    workRate: number
    workType: string
    branchId: number
  }
  const u: user2 = {
    email: user.email,
    username: user.username,
    password: user.password,
    fullName: user.fullName,
    gender: user.gender,
    role: user.role,
    image: user.image,
    workRate: user.workRate,
    workType: user.workType,
    branch: user.branch,
    branchId: user.branch?.id!
  }
  return http.post('/users/checkUser', u)
}

export default { addUser, updateUser, delUser, getUser, getUsers, getUsersByBranch,whileUser,getView}
