import type { BillCost } from '@/types/BillCost'
import http from './http'

function addBillCost(billcost: BillCost & { files: File[] }) {
  const formData = new FormData()
  formData.append('typebillcost', JSON.stringify(billcost.typebillcost))
  formData.append('billtotal', billcost.billtotal.toString())
  formData.append('user', JSON.stringify(billcost.user))
  // // เพิ่มวันที่ปัจจุบันในรูปแบบ 'YYYY-MM-DD'
  const currentDate = new Date().toISOString().split('T')[0]
  formData.append('date', currentDate)
  // formData.append('Date', billcost.Date)
  // formData.append('Time', billcost.Time)
  // เพิ่มเวลาปัจจุบันในรูปแบบ 'HH:mm:ss'
  const currentTime = new Date().toLocaleTimeString('en-US', { hour12: false })
  formData.append('time', currentTime)
  formData.append('branch', JSON.stringify(billcost.branch))

  if (billcost.files && billcost.files.length > 0) formData.append('file', billcost.files[0])
  return http.post('/billcosts', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateBillCost(billcost: BillCost & { files: File[] }) {
  console.log(billcost)
  const formData = new FormData()
  formData.append('typebillcost', JSON.stringify(billcost.typebillcost))
  formData.append('billtotal', billcost.billtotal.toString())
  formData.append('user', JSON.stringify(billcost.user))
  formData.append('date', billcost.date)
  formData.append('time', billcost.time)
  formData.append('branch', JSON.stringify(billcost.branch))
  if (billcost.files && billcost.files.length > 0) formData.append('file', billcost.files[0])
  return http.post(`/billcosts/${billcost.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delBillCost(billcost: BillCost) {
  return http.delete(`/billcosts/${billcost.id}`)
}

function getBillCost(id: number) {
  return http.get(`/billcosts/${id}`)
}

function getBillCostsByType(TypebillcostId: number) {
  return http.get('/billcosts/TypebillcostId/' + TypebillcostId)
}

function getBillCosts() {
  return http.get('/billcosts')
}

function getBillCostsByUser(userId: number) {
  return http.get(`/billcosts/user/${userId}`)
}

function getBillCostsByBranch(branchId: number) {
  return http.get(`/billcosts/branch/${branchId}`)
}

function getBillCostsreportTotalCostByType() {
  return http.get(`/billcosts/reportTotalCostByType`)
}

function getBillCostsreportTotalCostByTypeAndBranch(TypebillcostId: string, branchId: string) {
  return http.get(`/billcosts/reportTotalcostbytypeandbranc/${TypebillcostId}/${branchId}`)
}

function getBillCostsreportTotalCostByUserAndType() {
  return http.get(`/billcosts/reportTotalCostByUserAndType`)
}

function GetBillTotalByBranchAndTypePerYear(branchId: number, year: number) {
  return http.get(`/billcosts/GetBillTotalByBranchAndTypePerYear/${branchId}/${year}`)
}

// function GetBillTotalByBranchAndTypePerMonth2(branchId: number, month_year: string) {
//   return http.get(
//     `/billcosts/GetBillTotalByBranchAndTypePerMonth/${branchId}/${JSON.stringify(month_year)}`
//   )
// }

function GetBillTotalByBranchAndTypePerMonth(branchId: number) {
  return http.get(`/billcosts/GetBillTotalByBranchAndTypePerMonth/${branchId}`)
}

export default {
  addBillCost,
  updateBillCost,
  delBillCost,
  getBillCost,
  getBillCosts,
  getBillCostsByType,
  getBillCostsByUser,
  getBillCostsByBranch,
  getBillCostsreportTotalCostByType,
  getBillCostsreportTotalCostByTypeAndBranch,
  getBillCostsreportTotalCostByUserAndType,
  GetBillTotalByBranchAndTypePerYear,
  // GetBillTotalByBranchAndTypePerMonth2
  GetBillTotalByBranchAndTypePerMonth
}
