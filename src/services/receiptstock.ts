
import type { ReceiptStock } from '@/types/ReceiptStock'
import http from './http'
import type { ReceiptStockItem } from '@/types/ReceiptStockItem';
type ReceiptStockDTO={
    receiptStockItems:{
        stockId:number;
        unitPerItem: number;
        totalprice: number;
    }[];
    userId: number;
    date: Date;
    idreceipt:string;
    nameplace:string;
    total:number
}

function addReceiptStock(receiptstock: ReceiptStock,receiptStockItems:ReceiptStockItem[],DATE:string) {
   const receiptStockDTO:ReceiptStockDTO = {
     receiptStockItems: [],
     userId: 0,
     date: new Date,
     idreceipt:'',
     nameplace:'',
     total:0
   }
   receiptStockDTO.userId = receiptstock.userId
   receiptStockDTO.idreceipt = receiptstock.idreceipt
   receiptStockDTO.nameplace = receiptstock.nameplace
   receiptStockDTO.date = new Date(DATE)
   receiptStockDTO.total = receiptstock.total
   receiptStockDTO.receiptStockItems = receiptStockItems.map((item)=>{
    return {
      stockId:item.stockId,
      balance:item.balance,
      unitPerItem:item.unitPerItem,
      totalprice:item.totalprice
    }
   })
  return http.post('/receiptstocks', receiptStockDTO)
}

function updateReceiptStock(receiptstock: ReceiptStock) {
  return http.patch(`/receiptstocks/${receiptstock.id}`, receiptstock)
}

function delReceiptStock(receiptstock: ReceiptStock) {
  return http.delete("/receiptstocks/"+receiptstock.id)
}

function getReceiptStock(id: number) {
  return http.get(`/receiptstocks/${id}`)
}

function getReceiptStocks() {
  return http.get('/receiptstocks')
}
function getReceiptStocksByBranch(branchId:number) {
  return http.get('/receiptstocks/getByBranch/branch/Branch/'+branchId)
}

export default {
  addReceiptStock,
  updateReceiptStock,
  delReceiptStock,
  getReceiptStock,
  getReceiptStocks,
  getReceiptStocksByBranch
}
