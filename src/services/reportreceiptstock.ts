import http from './http'

function reportDay(day:string) {
  return http.get('/receiptstocks/reportDays/Days/'+day)
}
function reportCurrent(current:string) {
  return http.get('/receiptstocks/reportCurrent/current/'+current)
}
function reportMonth(month:string) {
  return http.get('/receiptstocks/reportMonth/Month/'+month)
}
function reprotYear() {
  return http.get('/receiptstocks/reportYear/Year')
}
export default { reportDay,reportMonth,reprotYear,reportCurrent}
