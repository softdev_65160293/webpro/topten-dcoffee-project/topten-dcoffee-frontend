import type { Branch } from '@/types/Branch'
import http from './http'
import type { Promotion } from '@/types/Promotion'
import type { Receipt } from '@/types/Receipt'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { User } from '@/types/User'
import type { Member } from '@/types/Member'
import member from './member'

type ReceiptDto = {
  receiptItems: {
    productId: number
    unit: number
    sweet_level: string
    gsize: string
    type: string
  }[]
  promotions?: Promotion[]
  user?: User
  member?: Member
  branch?: Branch
  branchId?: number
  date: string
  time: string
  channel: string
  paymentType: string
  receivedAmount: number
  change: number
  total_discount: number
  promotion_discount: number
  member_discount: number
  total_before: number
}

function addReceipt(receipt: Receipt, receiptItems: ReceiptItem[], date: string, time: string) {
  console.log(receipt)

  const receiptDto: ReceiptDto = {
    receiptItems: [],
    date: '',
    time: '',
    channel: '',
    paymentType: '',
    receivedAmount: 0,
    change: 0,
    branchId: 0,
    total_discount: 0,
    promotion_discount: 0,
    member_discount: 0,
    total_before: 0
  }
  const dateParts = receipt.date.split(',')[0].split('/')
  const month = dateParts[0]
  const day = dateParts[1]
  const year = dateParts[2]
  console.log('kist')

  console.log(year)
  console.log(month)
  console.log(day)
  console.log('-+-------')
  const dateObject = new Date(`${year}-${month}-${day}`)

  // แปลงวัตถุ Date เป็นรูปแบบ "yyyy-mm-dd"
  const formattedDate = `${year}-${month}-${day}`
  console.log(formattedDate)

  receiptDto.branch = receipt.branch
  receiptDto.branchId = receipt.branch?.id
  receiptDto.user = receipt.user
  receiptDto.member = receipt.member
  receiptDto.member_discount = parseInt(receipt.member_discount.toString())
  receiptDto.change = receipt.change
  receiptDto.channel = receipt.channel
  receiptDto.date = formattedDate
  receiptDto.paymentType = receipt.paymentType
  receiptDto.total_before = receipt.total_before
  receiptDto.promotion_discount = receipt.promotion_discount
  receiptDto.total_discount = receipt.member_discount + receipt.promotion_discount
  receiptDto.time = time.trim()
  receiptDto.receivedAmount = receipt.receivedAmount
  receiptDto.promotions = receipt.promotions

  receiptDto.receiptItems = receiptItems.map((item) => {
    return {
      productId: item.productId,
      unit: item.unit,
      sweet_level: item.sweet_level,
      gsize: item.gsize,
      type: item.type
    }
  })

  console.log(receiptDto)
  return http.post('/receipts', receiptDto)
}

function getReceipts() {
  return http.get('/receipts')
}

function report(type: string) {
  return http.get(`/receipts/report/${type}`)
}

function getDataYear(year: string) {
  return http.get(`/receipts/owner/${year}`)
}

function getReceiptsByBranch(branchId: number) {
  return http.get(`/receipts/branch/${branchId}`)
}

function getReceipt(id: number) {
  return http.get(`/receipts/${id}`)
}

function rmReceipt(receipt: Receipt) {
  return http.delete(`/receipts/${receipt.id}`)
}

function getDataYearBranch(branchId: string) {
  return http.get(`/receipts/report/${branchId}`)
}

function getSalesDaily(id: number, month: string) {
  return http.get(`/receipts/sales/day/${id}/${month}`)
}

function getSalesMonth(id: number) {
  return http.get(`/receipts/sales/month/${id}`)
}

function getSalesYear(id: number) {
  return http.get(`/receipts/sales/year/${id}`)
}

function getReceiptsByMember(memberId: number) {
  return http.get(`/receipts/memberId/${memberId}`)
}

export default {
  addReceipt,
  getReceipts,
  getReceiptsByBranch,
  report,
  getReceipt,
  rmReceipt,
  getDataYear,
  getDataYearBranch,
  getSalesDaily,
  getSalesMonth,
  getSalesYear,
  getReceiptsByMember
}
