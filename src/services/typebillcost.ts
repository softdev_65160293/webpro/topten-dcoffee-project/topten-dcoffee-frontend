import type { Typebillcost } from '@/types/Typebillcost'
import http from './http'

function addTypebillcost(typebillcost: Typebillcost) {
  return http.post('/typebillcosts', typebillcost)
}

function updateTypebillcost(typebillcost: Typebillcost) {
  return http.patch(`/typebillcosts/${typebillcost.id}`, typebillcost)
}

function delTypebillcost(typebillcost: Typebillcost) {
  return http.delete(`/typebillcosts/${typebillcost.id}`)
}

function getTypebillcost(id: number) {
  return http.get(`/typebillcosts/${id}`)
}

function getTypebillcosts() {
  return http.get('/typebillcosts')
}

export default {
  addTypebillcost,
  updateTypebillcost,
  delTypebillcost,
  getTypebillcost,
  getTypebillcosts
}
