import type { Branch } from '@/types/Branch'
import http from './http'

type BranchDto = {
  name: string
  Address: string
  managerId?: number
  tel: string
}

function addBranch(branch: Branch) {
  const branchDto: BranchDto = {
    name: branch.name,
    Address: branch.Address,
    tel: branch.tel,
    managerId: branch.manager.id
  }
  return http.post('/branchs', branchDto)
}

function updateBranch(branch: Branch) {
  const branchDto: BranchDto = {
    name: branch.name,
    Address: branch.Address,
    tel: branch.tel,
    managerId: branch.manager.id
  }
  console.log(branchDto)
  return http.patch(`/branchs/${branch.id}`, branchDto)
}

function delBranch(branch: Branch) {
  return http.delete(`/branchs/${branch.id}`)
}

function getBranch(id: number) {
  return http.get(`/branchs/${id}`)
}

function getBranchs() {
  return http.get('/branchs')
}

function getBranchByname(name: string) {
  return http.get(`/branchs/name/${name}`,)
}

export default {
  delBranch,
  getBranch,
  getBranchs,
  addBranch,
  updateBranch,
  getBranchByname
}
