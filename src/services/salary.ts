import type { Salary } from '@/types/Salary'
import http from './http'

function addSalary(salary: Salary) {
  return http.post('/salarys', salary)
}

function updateSalary(salary: Salary) {
  return http.post(`/salarys/${salary.id}`, salary)
}

function delSalary(salary: Salary) {
  return http.delete(`/salarys/${salary.id}`)
}

function getSalary(id: number) {
  return http.get(`/salarys/${id}`)
}

function getSalaryByUserId(id: number) {
  return http.get(`/salarys/users/${id}`)
}

function getSalaryByUserBranch(BranchId: number) {
  return http.get(`/salarys/branch/${BranchId}`)
}

function getSalaryByBranchId(id: number) {
  console.log(id)
  return http.get(`/salarys/branch/${id}`)
}

function getSalarys() {
  return http.get('/salarys')
}

export default {
  delSalary,
  getSalaryByUserId,
  getSalary,
  getSalarys,
  addSalary,
  updateSalary,
  getSalaryByBranchId,
  getSalaryByUserBranch
}
