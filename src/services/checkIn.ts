import type { CheckIn } from '@/types/CheckIn'
import http from './http'
import type { User } from '@/types/User'
import salary from './salary'
import type { Salary } from '@/types/Salary'

type checkInOut = {
  id?: number
  user: User
  date: string
  checkIn: string
  checkOut?: string
  salary?: Salary
  status?: boolean
}

type checkSalary = {
  id?: number
  checkOut?: string
  salary?: Salary
  salaryIdd?: number
  status?: boolean
}

function checkIn(checkIn: CheckIn) {
  const checkInOut: checkInOut = {
    user: checkIn.user,
    date: checkIn.date,
    checkIn: checkIn.checkIn
  }
  return http.post('/checkIns', checkInOut)
}

function checkOut(checkIn: CheckIn) {
  const checkInOut: checkInOut = {
    id: checkIn.id,
    user: checkIn.user,
    date: checkIn.date,
    checkIn: checkIn.checkIn,
    checkOut: checkIn.checkOut
  }
  console.log(checkInOut)

  return http.post(`/checkIns/checkOut/${checkIn.id}`, checkInOut)
}

function getCheckIn(id: number) {
  return http.get(`/checkIns/${id}`)
}
function getSumHour() {
  return http.get(`/checkIns/sumHour`)
}
function getCheckInByUserId(id: number) {
  return http.get(`/checkIns/users/${id}`)
}

function getLastCheckIn(id: number) {
  return http.get(`/checkIns/user/${id}`)
}

function getAllCheckIn(id: number) {
  return http.get(`/checkIns/users/${id}`)
}
function reportMonth(month: string) {
  return http.get(`/checkIns/month/${month}`)
}
function reportMonthByBranch(month: string, id: number) {
  return http.get(`/checkIns/month/${month}/${id}`)
}
function reportYear(year: number) {
  return http.get(`/checkIns/year/${year}`)
}
function reportYearByBranch(year: number, id: number) {
  return http.get(`/checkIns/year/${year}/${id}`)
}
function reportDay(day: string) {
  return http.get(`/checkIns/day/${day}`)
}
function reportDayByBranch(day: string, id: number) {
  return http.get(`/checkIns/day/${day}/${id}`)
}
function getBranch(id: number) {
  return http.get(`/checkIns/branch/${id}`)
}

function getCheckIns() {
  return http.get('/checkIns')
}
function getCheckInsSP() {
  return http.get('/checkIns/getCheckIn')
}

function updateCheck(check: CheckIn) {
  const checkSalary: checkSalary = {
    id: check.id,
    salary: check.salary,
    status: check.status
  }
  return http.patch(`/checkIns/${check.id}`, checkSalary)
}

export default {
  checkIn,
  updateCheck,
  checkOut,
  getCheckIn,
  getCheckInByUserId,
  getCheckIns,
  getLastCheckIn,
  getCheckInsSP,
  getAllCheckIn,
  getBranch,
  reportMonth,
  reportMonthByBranch,
  reportYear,
  reportYearByBranch,
  reportDay,
  reportDayByBranch,
  getSumHour
}
