import type { Stock } from './Stock'

type ReceiptStockItem = {
  id: number
  name: string
  minimum: number
  balance: number
  unit: string
  price: number
  totalprice: number
  unitPerItem: number
  stockId: number
  stock?: Stock
}

export { type ReceiptStockItem }
