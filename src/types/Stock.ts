import type { Branch } from "./Branch"


type Stock = {
  id?: number
  name: string
  minimum: number
  balance: number
  unit: string
  price: number
  branchId:number
  branch?:Branch
}

export { type Stock }
