import type { Branch } from './Branch'
import type { ReceiptStockItem } from './ReceiptStockItem'
import type { User } from './User'

type ReceiptStock = {
  id?: number
  idreceipt: string
  nameplace: string
  Date: string
  total: number
  userId: number
  user?: User
  receiptStockItems?: ReceiptStockItem[]
  branchId:number
  branch?:Branch
}

export type { ReceiptStock }
