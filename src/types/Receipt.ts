import type { Branch } from './Branch'
import type { Member } from './Member'
import type { Promotion } from './Promotion'
import type { ReceiptItem } from './ReceiptItem'
import type { User } from './User'

type Receipt = {
  id?: number
  date: string
  time: string
  total_before: number
  member_discount: number
  promotion_discount: number
  totalDiscount: number
  total: number
  receivedAmount: number
  change: number
  paymentType: string
  channel: string
  qty: number
  userId?: number
  user?: User
  memberId?: number
  staffId?: number
  member?: Member
  promotions?: Promotion[]
  receiptItems: ReceiptItem[]
  branch?: Branch
  memberName?: string
}

export type { Receipt }