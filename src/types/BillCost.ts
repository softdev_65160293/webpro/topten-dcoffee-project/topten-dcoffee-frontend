import type { Branch } from './Branch'
import type { Typebillcost } from './Typebillcost'
import type { User } from './User'


type BillCost = {
  id?: number
  typebillcost: Typebillcost
  user?: User
  date: string // date
  time: string // date(time)
  billtotal: number
  branch?: Branch
  image: string
}
function getImageUrl(billcost: BillCost) {
  return `http://localhost:3000/images/billcosts/${billcost.image}`
}

function getImageUrlID(id: number) {
  return `http://localhost:3000/images/billcosts/${id}`
}

export { type BillCost, getImageUrl, getImageUrlID }
