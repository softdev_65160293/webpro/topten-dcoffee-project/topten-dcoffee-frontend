import type { CheckStockItem } from './CheckStockItem'
import type { Stock } from './Stock'
import type { User } from './User'

type CheckStock = {
  id?: number
  date: string // date
  userId:number
  user?: User
  checkStockItems:CheckStockItem[]
  branchId:number
}

export { type CheckStock }
