import type { Salary } from './Salary'
import type { User } from './User'

type CheckIn = {
  id?: number
  user: User
  date: string
  checkIn: string
  checkOut: string
  hour: number
  status?: false | true
  salary?: Salary
}

export { type CheckIn }
