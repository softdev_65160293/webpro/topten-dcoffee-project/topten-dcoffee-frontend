import type { Product } from './Product'

type ReceiptItem = {
  id?: number
  name: string
  price: number
  unit: number
  type: string
  gsize: string
  sweet_level: string
  productId: number
  product?: Product
}

export { type ReceiptItem }