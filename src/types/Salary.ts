import type { CheckIn } from './CheckIn'
import type { Stock } from './Stock'
import type { User } from './User'

type Salary = {
  id?: number
  payDate: Date
  totalHour?: number
  totalPrice?: number
  managerId: number
  paymentMethod: string
  status: boolean
  user?: User
  checkIn?: CheckIn[]
}

export { type Salary }
