import type { Branch } from './Branch'
type User = {
  id?: number
  email: string
  username: string
  password: string
  fullName: string
  branch?: Branch
  gender: string
  role: string
  image: string
  workRate: number
  workType: string
  paidedHour?: number
  unpaidHour?: number
  status?: boolean
}

export type { User }
