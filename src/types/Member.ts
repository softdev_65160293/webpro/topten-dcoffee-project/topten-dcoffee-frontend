type Member = {
  id?: number
  email: string
  username: string
  password: string
  name: string
  tel: string
  point: number
  gender: string
  image: string
}

export type { Member }
