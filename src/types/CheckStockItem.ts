import type { Branch } from './Branch'
import type { Stock } from './Stock'
import type { User } from './User'

type CheckStockItem = {
    id?: number
    name: string
    minimum: number
    balance: number
    unit: string
    price: number
    stock?:Stock
    stockId:number
    branchId:number
    branch?: Branch
    amount:number
}

export { type CheckStockItem }
