import type { Product } from './Product'

type Promotion = {
  id?: number
  name: string
  condition: string
  start: string
  end: string
  status?: boolean
  discount: number
  products: Product[]
  image: string
}

function getImageUrl(promotion: Promotion) {
  return `http://localhost:3000/images/promotions/${promotion.image}`
}

export type { Promotion, getImageUrl }
